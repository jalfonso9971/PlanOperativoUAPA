<?php

session_start();


?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es"> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Plan Operativo</title>
    
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="vistas/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="vistas/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    
    <script src="vistas/plugins/sweetalert2/sweetalert2.all.js"></script>
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">


        <?php //$_SESSION['iniciarSesion']="ok";

        //session_destroy();

        if(isset($_SESSION['iniciarSesion']))
            {

                if($_SESSION['iniciarSesion']=="ok")//$_SESSION['validarSesion']=="ok"
                {
            ?>

                        <!-- NAVIGATION -->
                        <?php include 'modulos/nav.php'; ?>
                        <!-- /NAVIGATION -->


                        <!-- header -->
                        <?php //include 'modulos/header.php'; ?>
                        <!-- header -->

            <?php

            	       /*=============================================
                        contenido dinamico
                        =============================================*/
                        $rutas = array();

                        $ruta = null;

                        if(isset($_GET["ruta"]))
                        {

                            $rutas = explode("/", $_GET["ruta"]);

                            $item = "ruta";

                            $valor = $rutas[0];

                            $category = Funtion::before("-",$valor);

                            switch ($category) {

                                case 'ObjetivoEstratejico':

                                    $valor = "ObjetivoEstratejico";
                                    break;

                                case 'objetivostacticos':

                                    $valor = "objetivostacticos";
                                    break;
                                case 'lineasactuacion':

                                        $valor = "lineasactuacion";
                                        break;
                                case 'Metas':

                                        $valor = "Metas";
                                        break;
                                case 'Actividades':

                                        $valor = "Actividades";
                                        break;
                                case 'rolesadd':

                                        $valor = "rolesadd";
                                        break;
                                case 'tareaactividades':

                                        $valor = "tareaactividades";
                                        break;
                                case 'Reportes':

                                        $valor = "Reportes";
                                        break;
                                case 'departamento':

                                        $valor = "departamento";
                                        break;

                            }


                            /*=============================================
                            lista blanca de url amigables
                            =============================================*/

                            if (
                                $valor == "home" ||
                                $valor == "departamento" ||
                                $valor == "usuarios" ||
                                $valor == "Directores" ||
                                $valor == "Ejes" ||
                                $valor == "Metas" ||
                                $valor == "rolesadd" ||
                                $valor == "ObjetivoEstratejico" ||
                                $valor == "lineasactuacion" ||
                                $valor == "objetivostacticos" ||
                                $valor == "Actividades" ||
                                $valor == "tareaactividades" ||
                                $valor == "Evidencias" ||
                                $valor == "exit" ||
                                $valor == "Actividades" ||
                                $valor == "profile" ||
                                $valor == "bitacora" ||
                                $valor == "PanelAdmin" ||
                                $valor == "Dashboard" ||
                                $valor == "PostReglas" ||
                                $valor == "pdfPrestamo" ||
                                $valor == "Reportes" ||
                                $valor == "ReportePrestamosRealizados" ||
                                $valor == "renta" ||
                                $valor == "RentaDetails" ||
                                $valor == "RentaPago" ||
                                $valor == "cuentas" ||
                                $valor == "demo" ||
                                $valor == "AccesoDenegado")
                            {

                                include 'modulos/'.$valor.'.php';

                            }else{

                                include 'modulos/error404.php';
                            }

                        }else{//$_GET["ruta"]

                            include 'modulos/home.php';
                        }


                    /*<!-- Footer Area
                     ========================================== -->*/


                    include 'modulos/footer.php';

            echo "</div>";


                }//SesionOK

            }else{ //validarSesion


                 echo '<!-- Content Wrapper -->
                    <div id="content-wrapper" class="d-flex flex-column">

                        <!-- Main Content -->
                        <div id="content">';

                            $rutas = array();

                            $ruta = null;

                            if(isset($_GET["ruta"]))
                            {

                                $rutas = explode("/", $_GET["ruta"]);

                                $item = "ruta";

                                $valor = $rutas[0];

                                /*=============================================
                                lista blanca de url amigables
                                =============================================*/

                                if ($rutas[0] == "login" ||
                                    $rutas[0] == "lostPassword" ||
                                    $rutas[0] == "registro"||
                                    $rutas[0] == "licencia")
                                {

                                    include 'modulos/'.$rutas[0].'.php';

                                }else{

                                    header('Location: login');

                                    exit;
                                }

                            }else{

                                include "vistas/modulos/login.php";
                            }
                echo '  </div>
                    </div>';

            }
?>

    </div><!-- wrapper -->


    <!-- jQuery -->
    <script src="vistas/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="vistas/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="vistas/dist/js/adminlte.min.js"></script>
    <!-- Data table -->
    <script src="vistas/plugins/DataTable/datatables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'pageLength',
                    'pdf',
                    'excel',
                    'print'
                ]
            } );

            $('#example2').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'pageLength',
                    'pdf',
                    'excel',
                    'print'
                ]
            } );

          $('#TableClientes').DataTable();
      } );
    </script>


    <!-- custom scripts -->
    <script src="vistas/js/usuarios.js"></script>
    <script src="vistas/js/evidencias.js"></script>

</body>
</html>

