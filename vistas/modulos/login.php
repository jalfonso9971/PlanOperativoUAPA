<?php if (isset($_SESSION["iniciarSesion"])) {echo '<script>window.location = "home";</script>'; }?>

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row justify-content">
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Registrar</h1>
                  </div>
                  <form class="user" method="post">
                    <div class="form-group">
                      <input type="text" name="ingUsuario" class="form-control form-control-user" aria-describedby="Nombre de usuario" placeholder="Nombre de usuario" required="">
                    </div>
                    <div class="form-group">
                      <input type="password" name="ingPassword" class="form-control form-control-user" placeholder="Contraseña" required="">
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block"> Ingresar</button>

                    <?php

      				        $login = new ControladorUsuarios();
      				        $login -> ctrIngresoUsuario();

                    ?>

                  </form>
                  <hr>
                </div>
              </div>
              <div class="col-lg-6 d-none d-lg-block bg-login-image">
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

