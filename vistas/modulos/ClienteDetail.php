<?php

    $ruta               = null;

    @$rutas             = explode("/", $_GET["ruta"]);

    $valor              = $rutas[0];

    $IdCliente          = Funtion::after("-",$valor);
    $_SESSION["id_persona"] = $IdCliente;

    $Cliente            = ControladorPersona::ctrMostrarDetallePersona("id_persona",$IdCliente);

    $Documentos         = ControladorPersona::ctrMostrarDocumentos('id_persona', $IdCliente);

    $Prestamo           = ControladorPrestamo::ctrMostrarPrestamoProfile($_SESSION["id_empresa"], $IdCliente);

    $EditClientes       = new ControladorPersona();
    $EditClientes       -> ctrEditarPersona();

    $CargarDocumentos   = new ControladorPersona();
    $CargarDocumentos   -> ctrCargarDocumentos();

    $BorrarClientes     = new ControladorPersona();
    $BorrarClientes     -> ctrBorrarPersona();
    /////////////////////////////////
    //verificar acceso a este modulo
    /////////////////////////////////
    Funtion::Acceso(1);

?>

<link rel="stylesheet" href="vistas/plugins/upload-dropzone/basic.min.css">
<link rel="stylesheet" href="vistas/plugins/upload-dropzone/dropzone.min.css">

       <!-- Begin Page Content -->
        <div class="container-fluid">

          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="clientes">Clientes</a></li>
              <li class="breadcrumb-item active" aria-current="page"><?=$Cliente['nombre'].' '.$Cliente['apellido']?></li>
            </ol>
          </nav>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Datos de <?=$Cliente['nombre'].' '.$Cliente['apellido']?></h6>

              <?php if (in_array(3, $_SESSION["Acceso"])) { ?>

              <a href="" class="btn btn-outline-danger float-right ml-2" style="margin-top: -20px;" data-toggle="modal" data-target="#DelUser">
                  <i class="fas fa-trash-alt"></i>
              </a>

              <?php } if (in_array(2, $_SESSION["Acceso"])) { ?>

              <a href="" class="btn btn-outline-primary float-right" style="margin-top: -20px;" data-toggle="modal" data-target="#EditUser">
                  <i class="fas fa-user-edit"></i>
              </a>

              <?php } if (in_array(11, $_SESSION["Acceso"])) { ?>


              <a href="addprestamos-<?=$Cliente['id_persona']?>" class="btn btn-outline-primary float-right mr-2" style="margin-top: -20px;">
                  <i class="fas fa-donate"></i> Registrar Prestamo
              </a>

            <?php } ?>

            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-4">

                  <div class="card" style="width: 18rem;">

                    <?php if (empty($Cliente['imagen'])){ ?>

                      <img src="vistas/img/avatar.webp" class="card-img-top img-thumbnail" alt="Foto">

                    <?php }else{ ?>

                      <img src="<?='vistas/img/personas/'.$_SESSION["id_empresa"].'/'.$Cliente['id_persona'].'/'.$Cliente['imagen']?>" class="card-img-top img-thumbnail" alt="Foto">

                    <?php } ?>

                    <a href="" class="p-3" style="margin-top: -45px" data-toggle="modal" data-target="#EditLogo">
                      <i class="fas fa-camera"></i>
                    </a>

                    <div class="card-body text-center">
                      <h5 class="card-title"><?=$Cliente['nombre'].' '.$Cliente['apellido'].' ('.$Cliente['apodo'].')'?></h5>
                    </div>
                  </div>

                </div>
                <div class="col-md-8">

                  <ul class="list-group list-group-flush">
                    <li class="list-group-item"><strong><?=$Cliente['documento']?>:</strong> <?=$Cliente['cedula']?></li>
                    <li class="list-group-item"><strong>Fecha de nacimiento:</strong> <?=date("d/m/Y", strtotime($Cliente['fecha_nacimiento']))?></li>
                    <li class="list-group-item"><strong>Edad:</strong> <?=Funtion::Edad($Cliente['fecha_nacimiento'])?></li>
                    <li class="list-group-item"><strong>Sexo:</strong> <?=$Cliente['sexo']?></li>
                    <li class="list-group-item"><strong>Teléfono:</strong>
                      <a href="https://api.whatsapp.com/send?phone=1<?=Funtion::ClearString($Cliente['telefono'])?>" target="_black"><?=$Cliente['telefono']?></a>
                    </li>
                    <li class="list-group-item"><strong>Email:</strong> <?=$Cliente['email']?></li>
                    <li class="list-group-item"><strong>Ocupación:</strong> <?=$Cliente['ocupacion']?></li>
                    <li class="list-group-item"><strong>Teléfono Trabajo:</strong> <?=$Cliente['tel_trabajo']?></li>
                    <li class="list-group-item"><strong>Dirección:</strong> <?=$Cliente['direccion']?></li>
                    <li class="list-group-item"><strong>Observación:</strong> <?=$Cliente['observacion']?></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


        <div class="row">

          <?php if (in_array(4, $_SESSION["Acceso"])) { ?>

            <div class="col-md-6">
              <!-- Begin Page Content -->
              <div class="container-fluid">
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                  <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Documentos Archivados</h6>

                    <a href="#" class="btn btn-outline-primary float-right mr-2" style="margin-top: -20px;" data-toggle="modal" data-target="#AddDocumentos">
                        <i class="far fa-plus-square"></i>
                    </a>

                  </div>
                  <div class="card-body">

                  <div class="form-group">

                      <div class="row">

                        <table class="table table-striped dt-responsive" style="width:100%">
                          <thead>
                              <tr>
                                <th>ID</th>
                                <th>Doc.</th>
                                <th>Nombre</th>
                                <th>Fecha</th>
                                <th><i class="fas fa-sliders-h"></i></th>
                              </tr>
                            </thead>
                            <tbody>

                            <?php foreach ($Documentos as $key => $value) { ?>


                              <tr>
                                <td><?=$value['id_documento']?></td>
                                <td>

                                  <figure class="figure"  style="height: 40px">

                                    <?php

                                      $extension = new SplFileInfo($value['nombre_documento']);
                                      $extension = $extension->getExtension();

                                      switch ($extension) {
                                        case 'txt':
                                           echo '<i class="fas fa-file-alt fa-2x"></i>';
                                          break;

                                        case 'pdf':
                                           echo '<i class="fas fa-file-pdf fa-2x"></i>';
                                          break;

                                        case 'jpg';
                                        case 'jpeg';
                                        case 'png';
                                        case 'gif';
                                         echo '<img src="vistas/img/documentos/'.$value["id_persona"].'/'.$value["nombre_documento"].'" class="figure-img img-fluid rounded" alt="Documento" style="max-height: 100px">';
                                        break;

                                        default:
                                          echo '<i class="fas fa-file-alt fa-2x"></i>';
                                          break;
                                      }

                                    ?>
                                  </figure>

                                </td>
                                <td><?=$value['descripcion']?></td>
                                <td><?=date('d/m/Y', strtotime($value['fecha_documento']))?></td>
                                <td>

                                  <?php if (in_array(5, $_SESSION["Acceso"])) { ?>

                                    <a href="<?='vistas/img/documentos/'.$value["id_persona"].'/'.$value["nombre_documento"]?>" class="btn btn-secondary btn-primary" target="_blank">
                                      <i class="far fa-folder-open"></i>
                                    </a>

                                  <?php

                                    }

                                    if (in_array(6, $_SESSION["Acceso"])) {

                                    ?>

                                    <button type="button" class="btn btn-secondary btn-danger deleteimg" onclick="DelImgBlog(this)" data-value="<?=$value['id_documento']?>">
                                      <i class="far fa-trash-alt"></i>
                                    </button>

                                  <?php } ?>

                                </td>
                              </tr>

                            <?php } ?>

                          </tbody>
                        </table>

                      </div>

                  </div>


                  </div>
                </div>

              </div>
              <!-- /.container-fluid -->
            </div>

          <?php } if (in_array(7, $_SESSION["Acceso"])) { ?>

            <div class="col-md-6">
              <!-- Begin Page Content -->
              <div class="container-fluid">
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                  <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Prestamos</h6>

                  </div>
                  <div class="card-body">

                    <div class="table-responsive">

                      <table id="example" class="table table-striped dt-responsive nowrap" style="width:100%">
                          <thead>
                              <tr>
                                  <th>ID</th>
                                  <th>Monto</th>
                                  <th>Saldo</th>
                                  <th>Interés</th>
                                  <th>Cancelado</th>
                                  <th>Estado</th>
                                  <th><i class="fas fa-sliders-h"></i></th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($Prestamo as $key => $value) {

                              $porcentaje = (100-($value['saldo'] / $value['monto'])*100);

                            ?>
                            <tr>
                                <td><?=$value['id_prestamo']?></td>
                                <td><?=number_format($value['monto'],2)?></td>
                                <td><?=number_format($value['saldo'],2)?></td>
                                <td><?=$value['tasa_interes']?>%</td>
                                <td><?=round($porcentaje, 2)?>%</td>
                                <td><?=Funtion::EstadoPrestamo($value['estado'])?></td>
                                <td>

                                <?php if (in_array(9, $_SESSION["Acceso"])) { ?>

                                  <a href="prestamodetails-<?=$value['id_prestamo']?>" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Detalle">
                                    <i class="fas fa-eye"></i>
                                  </a>

                                <?php } if (in_array(10, $_SESSION["Acceso"])) { ?>

                                  <a href="CuotaPago-<?=$value['id_prestamo']?>" class="<?=($value['estado'] == 'P')?'disabled':''?> btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Nuevo pago">
                                    <i class="fas fa-file-invoice-dollar"></i>
                                  </a>

                                <?php } ?>

                                </td>
                              </tr>
                           <?php } ?>
                          </tbody>
                      </table>


                    </div>

                  </div>
                </div>

              </div>
              <!-- /.container-fluid -->
            </div>

          <?php } ?>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="EditUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Datos Personales</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                      <form class="user" method="post">
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                              <label class="label-form">Nombre</label>
                              <input type="text" name="PerNombre" class="form-control form-control-user" placeholder="Nombre" value="<?=$Cliente['nombre']?>">
                            </div>
                            <div class="col-sm-6">
                              <label class="label-form">Apellido</label>
                              <input type="text" name="PerApellido" class="form-control form-control-user" placeholder="Apellido" value="<?=$Cliente['apellido']?>">
                            </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Apodo</span>
                          </div>
                          <input type="text" name="apodo" class="form-control form-control-user" placeholder="Apodo" value="<?=$Cliente['apodo']?>">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                              <label class="label-form">Fecha de Nacimiento</label>
                              <input type="text" name="PerFecha_nacimiento" class="date form-control form-control-user" placeholder="Fecha de Nacimiento"  value="<?=date('d-m-Y',strtotime($Cliente['fecha_nacimiento']))?>">
                            </div>
                            <div class="col-sm-6">
                              <label class="label-form">Sexo</label>
                              <select name="PerSexo" class="form-control form-control-user">
                                <option class="disabled">Sexo</option>
                                <option value="Masculino" <?Funtion::select('Masculino',$Cliente['sexo'])?>>Masculino</option>
                                <option value="Femenina" <?Funtion::select('Femenina',$Cliente['sexo'])?>>Femenina</option>
                                <option value="Otro" <?Funtion::select('Otro',$Cliente['sexo'])?>>Otro</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                              <label class="label-form">Documento de identificación</label>
                              <div class="input-group">
                                <div class="input-group-append">
                                  <select name="TypeDocument" class="form-control" id="TypeDocument">
                                    <option value="Cedula" <?=Funtion::select('Cedula',$Cliente['documento'])?>>Cédula</option>
                                    <option value="Pasaporte" <?=Funtion::select('Pasaporte',$Cliente['documento'])?>>Pasaporte</option>
                                  </select>
                                </div>
                                <input type="numeber" name="PerCedula" id="PerCedula" class=" form-control form-control-user" value="<?=$Cliente['cedula']?>">
                              </div>
                            </div>
                            <div class="col-sm-6 mb-3 mb-sm-0">
                              <label class="label-form">Teléfono</label>
                              <input type="tel" name="PerTelefono" class="form-control form-control-user" placeholder="Teléfono" value="<?=$Cliente['telefono']?>"  onkeyup="mascara(this,'-',patron,true)">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                              <label>Ocupación</label>
                              <input type="numeber" name="ocupacion" class="form-control form-control-user" placeholder="Ocupación" value="<?=$Cliente['ocupacion']?>">
                            </div>
                            <div class="col-sm-6 mb-3 mb-sm-0">
                              <label>Télefono de Trabajo</label>
                              <input type="tel" name="TelTrabajo" class="form-control form-control-user" placeholder="Télefono de Trabajo" value="<?=$Cliente['tel_trabajo']?>"  onkeyup="mascara(this,'-',patron,true)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="label-form">Email</label>
                            <input type="email" name="PerEmail" class="form-control form-control-user" placeholder="Email" value="<?=$Cliente['email']?>">
                        </div>
                        <div class="form-group">
                            <label class="label-form">Dirección</label>
                            <input type="text" name="PerDireccion" class="form-control form-control-user" placeholder="Dirección"  value="<?=$Cliente['direccion']?>">
                        </div>
                        <div class="form-group">
                          <label>Observación</label>
                          <textarea name="observacion" class="form-control form-control-user" placeholder="Observación"><?=$Cliente['observacion']?></textarea>
                        </div>

                        <input type="hidden" name="id_persona" value="<?=$Cliente['id_persona']?>">
                        <input type="hidden" name="Edit_persona" value="Edit_persona">

                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Actualizar Datos
                        </button>
                        <hr>
                      </form>
              </div>
            </div>
          </div>
        </div>

        <!-- Logo -->
        <div class="modal fade" id="EditLogo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Actualizar Logo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                      <form class="user" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                          <input type="file" name="uploadedFile" class="form-control form-control-user" placeholder="Logo" required="">
                        </div>
                        <input type="hidden" value="<?=$IdCliente ?>" name="IdCliente">
                        <button type="submit" name="EditLogo" class="btn btn-primary btn-user btn-block">
                            Actualizar
                        </button>
                        <hr>
                      </form>
              </div>
            </div>
          </div>
        </div>

        <!-- Cargar documentos -->
        <div class="modal fade" id="AddDocumentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cargar Documentos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

                <label for="Mobile">Imagen (Max Files 20) Imagenes </label>
                <form action="#" class="dropzone" id="my-awesome-dropzone">
                  <div class="fallback">
                      <input name="file" type="file" multiple />
                  </div>
                </form>

                <form method="post" class="mt-3">
                    <label for="" class="text-left">Nombre del documento</label>
                    <input type="text" name="imgDescripcion" class="form-control" placeholder="Nombre del documento">

                    <div class="text-center">
                      <input type="submit" name="add_Documentos" class="btn btn-primary mt-3" value="Guardar Documentos">
                    </div>
                   <hr>
                </form>

              </div>
            </div>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="DelUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body text-center">

                    <h3>ESTA SEGURO QUE DESEA ELIMINAR ESTOS DATOS</h3>
                      <form class="user" method="post">

                        <input type="hidden" name="del_Cliente" value="del_Cliente">

                        <input type="hidden" name="id_persona" value="<?=$IdCliente?>">

                        <input type="hidden" name="nombre" value="<?=$Cliente['nombre'].' '.$Cliente['apellido']?>">

                        <button type="submit" class="btn btn-danger btn-user btn-block">
                            Eliminar datos
                        </button>
                        <hr>
                      </form>
              </div>
            </div>
          </div>
        </div>

        <script src="vistas/plugins/upload-dropzone/dropzone.min.js"></script>
        <script src="vistas/plugins/upload-dropzone/dropzone-amd-module.min.js"></script>
        <script>

          //https://www.dropzonejs.com/#configuration
          Dropzone.options.myAwesomeDropzone = {
            acceptedFiles: ".jpg,.jpeg,.png,.gif,.pdf,.txt", // The name that will be used to transfer the file
            maxFilesize: 10, // MB
            maxFiles: 20,
            thumbnailMethod: "contain",
            resizeHeight:800,
            addRemoveLinks: true,

            removedfile: function(file) {
                var name = file.name;

                $.ajax({
                          type: 'POST',
                          url: 'ajax/dropzone.php',
                          data: "id="+name,
                          dataType: 'html',
                          success: function(data) {
                                   //console.log(data);

                            }
                      });

                  var _ref;
                  return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;

                }

          };

        </script>