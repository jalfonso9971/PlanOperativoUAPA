<?php
  $respuesta = ControladorUsuarios::ctrMostrarUsuariosEmp($_SESSION["id_empresa"]);

  $Clientes  = ControladorPersona::ctrMostrarPersona("id_empresa",$_SESSION["id_empresa"]);

  $Cargo     = ControladorPermiso::ctrMostrarRoles();


  //envio datos para realizar el registro
  $login = new ControladorEmpleado();
  $login -> ctrCrearEmpleado();

  //envio datos para editar usuario
  $Edit = new ControladorUsuarios();
  $Edit -> ctrEditarUsuario();

  /////////////////////////////////
  //verificar acceso a este modulo
  /////////////////////////////////
  Funtion::Acceso(12);
 ?>

       <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Usuarios</h6>

              <?php if (in_array(13, $_SESSION["Acceso"])): ?>

                <a href="" class="btn btn-outline-primary float-right" style="margin-top: -20px;" data-toggle="modal" data-target="#AddUser">
                    <i class="fas fa-user-plus"></i>
                </a>

            <?php endif ?>

            </div>
            <div class="card-body">
              <div class="table-responsive">

                <table id="example" class="table table-striped dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Usuario</th>
                            <th>Nombre</th>
                            <th>Cedula</th>
                            <th>Email</th>
                            <th>Cargo</th>
                            <th>Fecha</th>
                            <th>Estado</th>
                            <th><i class="fas fa-sliders-h"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($respuesta as $key => $value) {
                        echo
                        '<tr>
                          <td>'.$value['id_usuario'].'</td>
                          <td>'.$value['usuario'].'</td>
                          <td>'.$value['nombre'].' '.$value['apellido'].'</td>
                          <td>'.$value['cedula'].'</td>
                          <td>'.$value['email'].'</td>
                          <td>'.$value['rol'].'</td>
                          <td>'.date('d/m/Y', strtotime($value['fecha'])).'</td>
                          <td>'.Funtion::EstadoUser($value['estado']).'</td>
                          <td>';
                          if (in_array(14, $_SESSION["Acceso"])):
                            echo '<a href="" class="btn btn-outline-primary" data-toggle="modal" data-target="#EditUser'.$value['id_usuario'].'">
                              <i class="far fa-edit"></i>
                            </a>';
                          endif;
                          if (in_array(15, $_SESSION["Acceso"])):
                           /* echo '<a href="" class="btn btn-outline-primary">
                              <i class="far fa-trash-alt"></i>
                            </a>';*/
                          endif;
                          echo '</td>
                              </tr>

                                <div class="modal fade" id="EditUser'.$value['id_usuario'].'" tabindex="-1" role="dialog" aria-hidden="true">
                                  <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                              <form class="user" method="post">

                                                <label for="Cargo">Cargo</label>
                                                <div class="input-group mb-3">
                                                  <input type="text" name="Cargo" value="'.$value['rol'].'" class="form-control" readonly="">
                                                  <input type="hidden" name="IdCargo" value="'.$value['id_roles'].'" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <input type="text" class="form-control form-control-user" readonly="" autocomplete="off" value="'.$value['usuario'].'">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" name="LogContrasena" class="form-control form-control-user" placeholder="Contraseña Nueva" autocomplete="off">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" name="LogConfirContrasena" class="form-control form-control-user" placeholder="Confirmar Contraseña" autocomplete="off">
                                                </div>
                                                <div class="form-group">
                                                    <select name="estado" class="form-control form-control-user">
                                                      <option value="1" '.Funtion::select(1,$value['estado']).'>Activo</option>
                                                      <option value="0" '.Funtion::select(0,$value['estado']).'>Inactivo</option>
                                                    </select>
                                                </div>
                                                <input type="hidden" name="passwordActual" value="'.$value['contrasena'].'">
                                                <input type="hidden" name="id_usuario" value="'.$value['id_usuario'].'">
                                                <button type="submit" name="editarUsuario" class="btn btn-primary btn-user btn-block">
                                                    Registrar Datos
                                                </button>
                                                <hr>
                                              </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>';
                       } ?>
                    </tbody>
                </table>


              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


        <?php if (in_array(13, $_SESSION["Acceso"])): ?>

        <!-- Modal -->
        <div class="modal fade" id="AddUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Datos Personales</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

                      <div class="text-center">
                        <h5>Datos de Ingreso al Sistema</h5>
                      </div>
                      <form class="user" method="post">

                        <label for="cliente">Persona</label>
                        <div class="input-group mb-3">
                          <input type="text" name="cliente" id="NombreCliente" value="" class="form-control" readonly="">
                          <input type="hidden" name="idcliente" id="IdCliente" value="" class="form-control">
                          <div class="input-group-append">
                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#SearchClient">
                              <i class="far fa-address-card"></i>
                            </button>
                          </div>
                        </div>

                        <label for="Cargo">Cargo</label>
                        <div class="input-group mb-3">
                          <input type="text" name="Cargo" id="NombreCargo" value="" class="form-control" readonly="">
                          <input type="hidden" name="IdCargo" id="IdCargo" value="" class="form-control">
                          <div class="input-group-append">
                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#SearchCargo">
                              <i class="fas fa-id-card-alt"></i>
                            </button>
                          </div>
                        </div>

                        <div class="form-group">
                            <input type="text" name="LogUsuario" id="nuevoUsuario" class="form-control form-control-user"placeholder="Usuario" required="" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <input type="password" name="LogContrasena" class="form-control form-control-user" placeholder="Contraseña" required="" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <input type="password" name="LogConfirContrasena" class="form-control form-control-user" placeholder="Confirmar Contraseña" required="" autocomplete="off">
                        </div>

                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Registrar Usuario
                        </button>
                        <hr>
                      </form>
              </div>
            </div>
          </div>
        </div>

        <!-- Search Client -->
        <div class="modal fade" id="SearchClient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Clientes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

                     <div class="table-responsive">

                <table id="TableClientes" class="table table-striped dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Cedula</th>
                            <th>Telefono</th>
                            <th><i class="fas fa-sliders-h"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($Clientes as $key => $value) {
                        echo
                        '<tr>
                          <td>'.$value['id_persona'].'</td>
                          <td><a href="#">'.$value['nombre'].' '.$value['apellido'].'</a></td>
                          <td>'.$value['cedula'].'</td>
                          <td>'.$value['telefono'].'</td>
                          <td>
                            <a href="#" class="btn btn-outline-primary">
                              <i class="fas fa-check-circle"></i>
                            </a>
                          </td>
                        </tr>
                        ';
                       } ?>
                    </tbody>
                </table>


              </div>

              </div>
            </div>
          </div>
        </div>

        <?php endif ?>

        <!-- Search Cargo -->
        <div class="modal fade" id="SearchCargo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cargo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

              <div class="table-responsive">

                <table id="TableCargo" class="table table-striped dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th><i class="fas fa-sliders-h"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($Cargo as $key => $value) {
                        echo
                        '<tr>
                          <td>'.$value['id_roles'].'</td>
                          <td>'.$value['nombre'].'</td>
                          <td>
                            <a href="#" class="btn btn-outline-primary">
                              <i class="fas fa-check-circle"></i>
                            </a>
                          </td>
                        </tr>
                        ';
                       } ?>
                    </tbody>
                </table>


              </div>

              </div>
            </div>
          </div>
        </div>