<?php
  $respuesta = ControladorPersona::ctrMostrarPersona("id_empresa",$_SESSION["id_empresa"]);

    //envio datos para realizar el registro
    $Clientes = new ControladorPersona();
    $Clientes -> ctrCrearPersona();

    $EditClientes = new ControladorPersona();
    $EditClientes -> ctrEditarPersona();

    /////////////////////////////////
    //verificar acceso a este modulo
    /////////////////////////////////
    Funtion::Acceso(1);
 ?>

       <!-- Begin Page Content -->
        <div class="container-fluid">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page">Clientes</li>
            </ol>
          </nav>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Clientes</h6>

              <?php if (in_array(8, $_SESSION["Acceso"])): ?>

                <a href="" class="btn btn-outline-primary float-right" style="margin-top: -20px;" data-toggle="modal" data-target="#AddUser">
                    <i class="fas fa-user-plus"></i>
                </a>

              <?php endif; ?>

            </div>
            <div class="card-body">
              <div class="table-responsive">

                <table id="example" class="table table-striped dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Apodo</th>
                            <th>Cedula</th>
                            <th>Telefono</th>
                            <th>Fecha de Registro</th>
                            <th><i class="fas fa-sliders-h"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php $ID = 1; foreach ($respuesta as $key => $value) {
                        echo
                        '<tr>
                          <td>'.$ID.'</td>
                          <td><a href="detail-'.$value["id_persona"].'">'.$value['nombre'].' '.$value['apellido'].'</a></td>
                          <td>'.$value['apodo'].'</td>
                          <td>'.$value['cedula'].'</td>
                          <td>'.$value['telefono'].'</td>
                          <td>'.date('d/m/Y', strtotime($value['fecha'])).'</td>
                          <td>
                            <a href="detail-'.$value["id_persona"].'" class="btn btn-outline-primary mr-1">
                              <i class="fas fa-eye"></i>
                            </a>';

                            if (in_array(2, $_SESSION["Acceso"])) {

                              echo '<a href="" class="btn btn-outline-primary" data-toggle="modal" data-target="#EditUser'.$value['id_persona'].'">
                                <i class="far fa-edit"></i>
                              </a>';

                            }

                            echo '
                            <!--
                            <a href="" class="btn btn-outline-primary">
                              <i class="fas fa-eye-slash"></i>
                            </a>
                            -->
                          </td>
                        </tr>';

                          if (in_array(2, $_SESSION["Acceso"])):

                            echo '
                                <!-- Modal -->
                                <div class="modal fade" id="EditUser'.$value['id_persona'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Datos Personales</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                              <form class="user" method="post">
                                                <div class="form-group row">
                                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                                      <label class="label-form">Nombre</label>
                                                      <input type="text" name="PerNombre" class="form-control form-control-user" placeholder="Nombre" required="" value="'.$value['nombre'].'">
                                                    </div>
                                                    <div class="col-sm-6">
                                                      <label class="label-form">Apellido</label>
                                                      <input type="text" name="PerApellido" class="form-control form-control-user" placeholder="Apellido" required="" value="'.$value['apellido'].'">
                                                    </div>
                                                </div>
                                                <div class="input-group mb-3">
                                                  <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">Apodo</span>
                                                  </div>
                                                  <input type="text" name="apodo" class="form-control form-control-user" placeholder="Apodo" value="'.$value['apodo'].'">
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                                      <label class="label-form">Fecha de Nacimiento</label>
                                                      <input type="text" name="PerFecha_nacimiento" class="date form-control form-control-user" placeholder="Fecha de Nacimiento" required="" value="'.date("d/m/Y",strtotime($value['fecha_nacimiento'])).'">
                                                    </div>
                                                    <div class="col-sm-6">
                                                      <label class="label-form">Sexo</label>
                                                      <select name="PerSexo" class="form-control form-control-user">
                                                        <option class="disabled">Sexo</option>
                                                        <option value="Masculino" '.Funtion::select('Masculino',$value['sexo']).'>Masculino</option>
                                                        <option value="Femenina" '.Funtion::select('Femenina',$value['sexo']).'>Femenina</option>
                                                        <option value="Otro" '.Funtion::select('Otro',$value['sexo']).'>Otro</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-6">
                                                      <label class="label-form">Documento de identificación</label>
                                                      <div class="input-group">
                                                        <div class="input-group-append">
                                                          <select name="TypeDocument" class="form-control" >
                                                            <option value="Cedula" '.Funtion::select('Cedula',$value['documento']).'>Cédula</option>
                                                            <option value="Pasaporte" '.Funtion::select('Pasaporte',$value['documento']).'>Pasaporte</option>
                                                          </select>
                                                        </div>
                                                        <input type="numeber" name="PerCedula" class=" form-control form-control-user" value="'.$value['cedula'].'">
                                                      </div>
                                                    </div>
                                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                                      <label class="label-form">Teléfono</label>
                                                      <input type="tel" name="PerTelefono" class="form-control form-control-user" placeholder="Teléfono" value="'.$value['telefono'].'">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-6">
                                                      <label>Ocupación</label>
                                                      <input type="numeber" name="ocupacion" class="form-control form-control-user" placeholder="Ocupación" value="'.$value['ocupacion'].'">
                                                    </div>
                                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                                      <label>Télefono de Trabajo</label>
                                                      <input type="tel" name="TelTrabajo" class="form-control form-control-user" placeholder="Télefono de Trabajo" value="'.$value['tel_trabajo'].'">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="label-form">Email</label>
                                                    <input type="email" name="PerEmail" class="form-control form-control-user" placeholder="Email" value="'.$value['email'].'">
                                                </div>
                                                <div class="form-group">
                                                    <label class="label-form">Dirección</label>
                                                    <input type="text" name="PerDireccion" class="form-control form-control-user" placeholder="Dirección" value="'.$value['direccion'].'">
                                                </div>
                                                <div class="form-group">
                                                  <label>Observación</label>
                                                  <textarea name="observacion" class="form-control form-control-user" placeholder="Observación">'.$value['observacion'].'</textarea>
                                                </div>

                                                <input type="hidden" name="id_persona" value="'.$value['id_persona'].'">
                                                <input type="hidden" name="Edit_persona" value="Edit_persona">

                                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                                    Actualizar Datos
                                                </button>
                                                <hr>
                                              </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            ';

                          endif;
                      $ID++; }

                      ?>
                    </tbody>
                </table>


              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

        <?php if (in_array(8, $_SESSION["Acceso"])): ?>

        <!-- Modal -->
        <div class="modal fade" id="AddUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Datos Personales</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                      <form class="user" method="post">
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                              <label>Nombre</label>
                              <input type="text" name="PerNombre" class="form-control form-control-user" placeholder="Nombre">
                            </div>
                            <div class="col-sm-6">
                              <label>Apellido</label>
                              <input type="text" name="PerApellido" class="form-control form-control-user" placeholder="Apellido">
                            </div>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Apodo</span>
                          </div>
                          <input type="text" name="apodo" class="form-control form-control-user" placeholder="Apodo">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                              <label for="">Fecha de Nacimiento</label>
                              <input type="date" name="PerFecha_nacimiento" class="form-control form-control-user date" placeholder="Fecha de Nacimiento">
                            </div>
                            <div class="col-sm-6">
                              <label for="">Sexo</label>
                              <select name="PerSexo" class="form-control form-control-user">
                                <option class="disabled" >Sexo</option>
                                <option value="Masculino">Masculino</option>
                                <option value="Femenina">Femenina</option>
                                <option value="Otro">Otro</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                              <label>Documento de identificación</label>
                              <div class="input-group">
                                <div class="input-group-append">
                                  <select name="TypeDocument" class="form-control" id="TypeDocument">
                                    <option value="Cedula" >Cédula</option>
                                    <option value="Pasaporte" >Pasaporte</option>
                                  </select>
                                </div>
                                <input type="numeber" name="PerCedula" id="PerCedula" class=" form-control form-control-user" placeholder="Cédula Ej. 000-0000000-0">
                              </div>
                            </div>
                            <div class="col-sm-6 mb-3 mb-sm-0">
                              <label>Télefono</label>
                              <input type="tel" name="PerTelefono" class="movil form-control form-control-user" placeholder="Télefono" onkeyup="mascara(this,'-',patron,true)">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                              <label>Ocupación</label>
                              <input type="numeber" name="ocupacion" class="form-control form-control-user" placeholder="Ocupación">
                            </div>
                            <div class="col-sm-6 mb-3 mb-sm-0">
                              <label>Télefono de Trabajo</label>
                              <input type="tel" name="TelTrabajo" class="form-control form-control-user" placeholder="Télefono de Trabajo" onkeyup="mascara(this,'-',patron,true)">
                            </div>
                        </div>
                        <div class="form-group">
                          <label>Email</label>
                          <input type="email" name="PerEmail" class="form-control form-control-user" placeholder="Email">
                        </div>
                        <div class="form-group">
                          <label>Dirección</label>
                          <input type="text" name="PerDireccion" class="form-control form-control-user" placeholder="Dirección">
                        </div>
                        <div class="form-group">
                          <label>Observación</label>
                          <textarea name="observacion" class="form-control form-control-user" placeholder="Observación"></textarea>
                        </div>

                        <input type="hidden" name="add_persona" value="add_persona">

                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Registrar Cuenta
                        </button>
                        <hr>
                      </form>
              </div>
            </div>
          </div>
        </div>

      <?php endif; ?>
