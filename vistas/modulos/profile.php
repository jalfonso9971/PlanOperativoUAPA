<?php
if (!isset($_SESSION["iniciarSesion"])) {

    echo '<script>window.location = "login";</script>';


}else{

    $user = ControladorUsuarios::ctrMostrarUsuariosProfile($_SESSION["id_user"]);

    $edit_user        = new ControladorUsuarios();
    $edit_user        -> ctrEditarUsuario();
}
?>

       <!-- Begin Page Content -->
        <div class="container-fluid">

          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page"><?=$_SESSION['usuario']?></li>
            </ol>
          </nav>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Datos de <?=$user['nombre'].' '.$user['apellido']?></h6>
            <!--
              <a href="" class="btn btn-outline-primary float-right" style="margin-top: -20px;" data-toggle="modal" data-target="#EditUser">
                  <i class="fas fa-user-edit"></i>
              </a>
            -->
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-4">

                  <div class="card" style="width: 18rem;">

                    <?php if (empty($user['imagen'])){ ?>

                      <img src="vistas/img/avatar.webp" class="card-img-top img-thumbnail" alt="Foto">

                    <?php }else{ ?>

                      <img src="<?='vistas/img/personas/'.$_SESSION["id_empresa"].'/'.$user['id_persona'].'/'.$user['imagen']?>" class="card-img-top img-thumbnail" alt="Foto">

                    <?php } ?>

                    <div class="card-body text-center">
                      <h5 class="card-title"><?=$user['nombre'].' '.$user['apellido']?></h5>
                    </div>
                  </div>

                </div>
                <div class="col-md-8">
                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th>Cédula:</th>
                        <td><?=$user['cedula']?></td>
                      </tr>
                      <tr>
                        <th>Fecha de nacimiento:</th>
                        <td><?=$user['fecha_nacimiento']?></td>
                      </tr>
                      <tr>
                        <th>Sexo:</th>
                        <td><?=$user['sexo']?></td>
                      </tr>
                      <tr>
                        <th>Teléfono:</th>
                        <td><?=$user['telefono']?></td>
                      </tr>
                      <tr>
                        <th>Email:</th>
                        <td><?=$user['email']?></td>
                      </tr>
                      <tr>
                        <th>Dirección:</th>
                        <td><?=$user['direccion']?></td>
                      </tr>
                      <tr>
                        <th>Cargo:</th>
                        <td><?=$user['cargo']?></td>
                      </tr>
                      <tr>
                        <th>Fecha de Ingreso:</th>
                        <td><?=date('d/m/Y',strtotime($user['fecha_inicio']))?></td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>