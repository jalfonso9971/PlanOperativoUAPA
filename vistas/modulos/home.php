
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
  
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <!-- Main content -->
    <section class="content ">
      <div class="container-fluid ">
        <!-- Small boxes (Stat box) -->
        <div class="row ">
          <div class="col-lg-12 col-12 ">
            

                  
            <body>
               

                <main role="main">

                  <!-- Main jumbotron for a primary marketing message or call to action -->
                  <div class="jumbotron d-none d-lg-block bg-login-image">
                    <div class="container">
                      <h1 class="display-3">|</h1>

                      <p style="color:#FFFFFF">El plan operativo del Departamento de Curso Final de Grado, constituye un instrumento fundamental para evaluar el estado situacional actual del departamento y encaminar acciones puntuales para validar aspectos eficientes y reencausar otros que no han evidenciado ser pertinentes.</p>
                     <!--  <p><a class="btn btn-primary btn-lg" href="#" role="button">Leer mas »</a></p> -->
                    </div>
                  </div>

                  <div class="container">
                    <!-- Example row of columns -->
                    <div class="row">
                      <div class="col-md-4">
                        <h2>Misión</h2>
                        <p>Completar la formación de los participantes en las diferentes carreras de la Universidad Abierta para Adultos, facilitando innovación, desarrollo, competitividad y gestores permanente de calidad y excelencia para su inserción en el mercado laboral con las competencias necesarias que demanda la sociedad actual.</p>
                        <!-- <p><a class="btn btn-secondary" href="#" role="button">Ver detalles »</a></p> -->
                      </div>
                      <div class="col-md-4">
                        <h2>Visión</h2>
                        <p>Ser un Departamento de Formación e innovación permanente de los participantes de la UAPA que cursan el último nivel a través de un equipo profesional y polivalente que incorpora metodología adecuada para lograr en los participantes las competencias necesarias y adecuadas con aplicación al puesto de trabajo </p>
                       <!--  <p><a class="btn btn-secondary" href="#" role="button">Ver detalles »</a></p> -->
                      </div>
                      <div class="col-md-4">
                        <h2>Valores</h2>
                        <p>Respeto</p>
                        <p>Autocrítica</p>
                        <p>Disciplina</p>
                        <p>Constancia</p>
                        <p>Integridad</p>
                       <!--  <p><a class="btn btn-secondary" href="#" role="button">Ver detalles »</a></p> -->
                      </div>
                    </div> 
                  </div> <!-- /container -->
                </main>              
             </body>
           </div>
          </div>          <!-- /.row -->
         </div>
        </main>
       </section>        
      </div>
    </div>
  </div>
</div>