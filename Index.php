<?php
require_once "controladores/plantilla.controlador.php";
require_once "controladores/usuarios.controlador.php";
require_once "controladores/persona.controlador.php";
require_once "controladores/roles.controlador.php";
require_once "controladores/bitacora.controlador.php";
require_once "controladores/reportes.controlador.php";
require_once "controladores/departamento.controlador.php";
require_once "controladores/directores.controlador.php";
require_once "controladores/ejes.controlador.php";
require_once "controladores/meta.controlador.php";
require_once "controladores/objetivosestratejicos.controlador.php";
require_once "controladores/lineasactuacion.controlador.php";
require_once "controladores/objetivostacticos.controlador.php";
require_once "controladores/actividades.controlador.php";
require_once "controladores/evidencias.controlador.php";
require_once "controladores/tareaactividades.controlador.php";
require_once "controladores/reportes.controlador.php";


require_once "modelos/usuarios.modelo.php";
require_once "modelos/persona.modelo.php";
require_once "modelos/roles.modelo.php";
require_once "modelos/bitacora.modelo.php";
require_once "modelos/reportes.modelo.php";
require_once "modelos/departamento.modelo.php";
require_once "modelos/directores.modelo.php";
require_once "modelos/ejes.modelo.php";
require_once "modelos/metas.modelo.php";
require_once "modelos/objetivosestratejicos.modelo.php";
require_once "modelos/lineasactuacion.modelo.php";
require_once "modelos/objetivostacticos.modelo.php";
require_once "modelos/actividades.modelo.php";
require_once "modelos/tareaactividades.modelo.php";
require_once "modelos/reportes.modelo.php";
require_once "modelos/evidencias.modelo.php";
require_once "modelos/funtion.php";


$plantilla = new ControladorPlantilla();
$plantilla -> ctrPlantilla();


?>