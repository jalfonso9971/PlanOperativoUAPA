<?php

require_once "conexion.php";

class Modeloobjetivostacticos{

	/*=============================================
	CREAR Objetivo
	=============================================*/

	static public function mdlRegistroobjetivostacticos($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("

			INSERT INTO $tabla ( Idobjestratejico, Nomobjtactico, descobjtactico)
			VALUES
				(
					:Idobjestratejico, :Nomobjtactico, :descobjtactico
				)
			");

		$stmt->bindParam(":Idobjestratejico", 			$datos['Idobjestratejico'], PDO::PARAM_INT);
		$stmt->bindParam(":Nomobjtactico", 				$datos['Nomobjtactico'], PDO::PARAM_STR);
		$stmt->bindParam(":descobjtactico", 			$datos['descobjtactico'], PDO::PARAM_STR);
		

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Editar Ejes
	=============================================*/

	static public function mdlEditarobjetivostacticos($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("UPDATE $tabla SET Nomobjtactico = :Nomobjtactico, descobjtactico = :descobjtactico WHERE Idobjtactico = :Idobjtactico ");

		$stmt->bindParam(":Nomobjtactico", 		$datos['Nomobjtactico'], PDO::PARAM_STR);
		$stmt->bindParam(":descobjtactico", 		$datos['descobjtactico'], PDO::PARAM_STR);
		$stmt->bindParam(":Idobjtactico", 		$datos['Idobjtactico'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Elimina Ejes
	=============================================*/

	static public function mdlEliminarobjetivostacticos($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("DELETE FROM $tabla WHERE Idobjtactico = :Idobjtactico ");

		$stmt->bindParam(":Idobjtactico", 		$datos['Idobjtactico'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR
	=============================================*/

	static public function mdlMostrarobjetivostacticos($tabla, $Idobjestratejicos){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE Idobjestratejico = :Idobjestratejico ");

		$stmt->bindParam(":Idobjestratejico", 					$Idobjestratejicos, PDO::PARAM_INT);

		$stmt -> execute();							

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}

	static public function mdlMostrarobjetivostacticosDashboard(){

		$stmt = Conexion::conectar()->prepare("
		SELECT
			obj_estratejicos.nomobjestratejico,
			obj_tactico.Nomobjtactico,
			obj_tactico.descobjtactico 
		FROM
			obj_estratejicos
			INNER JOIN obj_tactico ON obj_tactico.Idobjestratejico = obj_tactico.Idobjestratejico
			 ");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}
}
