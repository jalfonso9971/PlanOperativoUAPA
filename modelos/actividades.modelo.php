<?php

require_once "conexion.php";

class Modeloactividades{

	/*=============================================
	CREAR actividades
	=============================================*/

	static public function mdlRegistroactividades($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("

			INSERT INTO $tabla ( Idmeta, Nomactividades, descactividades, fechainicio, fechafin)
			VALUES
				(
					:Idmeta,
					:Nomactividades,
					:descactividades,
					:fechainicio,
					:fechafin
				)
			");

		$stmt->bindParam(":Idmeta", 				$datos['Idmeta'], PDO::PARAM_STR);
		$stmt->bindParam(":Nomactividades", 		$datos['Nomactividades'], PDO::PARAM_STR);
		$stmt->bindParam(":descactividades", 		$datos['descactividades'], PDO::PARAM_STR);
		$stmt->bindParam(":fechainicio", 			$datos['fechainicio'], PDO::PARAM_STR);
		$stmt->bindParam(":fechafin", 				$datos['fechafin'], PDO::PARAM_STR);
		

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}
	/*=============================================
	Editar Ejes
	=============================================*/

	static public function mdlEditaractividades($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("UPDATE $tabla SET fechainicio = :fechainicio, fechafin = :fechafin, descactividades = :descactividades, Nomactividades = :Nomactividades WHERE Idactividades = :Idactividades ");

		$stmt->bindParam(":descactividades", 		$datos['descactividades'], PDO::PARAM_STR);
		$stmt->bindParam(":Nomactividades", 		$datos['Nomactividades'], PDO::PARAM_STR);
		$stmt->bindParam(":Idactividades", 			$datos['Idactividades'], PDO::PARAM_INT);
		$stmt->bindParam(":fechainicio", 			$datos['fechainicio'], PDO::PARAM_STR);
		$stmt->bindParam(":fechafin", 				$datos['fechafin'], PDO::PARAM_STR);


		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Elimina Ejes
	=============================================*/

	static public function mdlEliminaractividades($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("DELETE FROM $tabla WHERE Idactividades = :Idactividades ");

		$stmt->bindParam(":Idactividades", 		$datos['Idactividades'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR
	=============================================*/

	static public function mdlMostraractividades($tabla, $Idmetas){

		$stmt = Conexion::conectar()->prepare(" SELECT * FROM $tabla WHERE Idmeta = :Idmeta ");

		$stmt->bindParam(":Idmeta", 					$Idmetas, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}

	static public function mdlMostraractividadesDashboard(){

		$stmt = Conexion::conectar()->prepare("
		SELECT
			metas.Nommeta,
			actividad.Nomactividades,
			actividad.descactividades 
		FROM
			metas
			INNER JOIN actividad ON actividad.Idmeta = metas.Idmeta
			 ");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}
}


