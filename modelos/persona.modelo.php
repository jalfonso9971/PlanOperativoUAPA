<?php

require_once "conexion.php";

class ModeloPersona{

	/*=============================================
	CREAR Persona
	=============================================*/

	static public function mdlRegistroPersona($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("

			INSERT INTO $tabla ( id_empresa, cedula, nombre, apellido, apodo, fecha_nacimiento, telefono, email, sexo, direccion, ocupacion, tel_trabajo, documento, observacion)
			VALUES
				(
					:id_empresa,
					:cedula,
					:nombre,
					:apellido,
					:apodo,
					:fecha_nacimiento,
					:telefono,
					:email,
					:sexo,
					:direccion,
					:ocupacion,
					:tel_trabajo,
					:documento,
					:observacion
				)
			");

		$stmt->bindParam(":id_empresa", 		$datos['IdEmpresa'], PDO::PARAM_INT);
		$stmt->bindParam(":cedula", 			$datos['PerCedula'], PDO::PARAM_STR);
		$stmt->bindParam(":nombre", 			$datos['PerNombre'], PDO::PARAM_STR);
		$stmt->bindParam(":apellido", 			$datos['PerApellido'], PDO::PARAM_STR);
		$stmt->bindParam(":apodo", 				$datos['apodo'], PDO::PARAM_STR);
		$stmt->bindParam(":fecha_nacimiento", 	$datos['PerFecha_nacimiento'], PDO::PARAM_STR);
		$stmt->bindParam(":telefono", 			$datos['PerTelefono'], PDO::PARAM_STR);
		$stmt->bindParam(":email", 				$datos['PerEmail'], PDO::PARAM_STR);
		$stmt->bindParam(":sexo", 				$datos['PerSexo'], PDO::PARAM_STR);
		$stmt->bindParam(":direccion", 			$datos['direccion'], PDO::PARAM_STR);
		$stmt->bindParam(":ocupacion", 			$datos['ocupacion'], PDO::PARAM_STR);
		$stmt->bindParam(":tel_trabajo", 		$datos['tel_trabajo'], PDO::PARAM_STR);
		$stmt->bindParam(":documento", 			$datos['documento'], PDO::PARAM_STR);
		$stmt->bindParam(":observacion", 		$datos['observacion'], PDO::PARAM_STR);

		if($stmt->execute()){

			return $lastId = $pdo->lastInsertId();

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR Persona
	=============================================*/

	static public function mdlMostrarPersona($tabla, $item, $valor, $id_empresa){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_empresa = $id_empresa AND $item = :$item ORDER BY id_persona DESC ");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_empresa = $id_empresa ORDER BY id_persona DESC ");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR Detalle de Persona
	=============================================*/
	static public function mdlMostrarDetallePersona($tabla, $item, $valor, $id_empresa){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_empresa = $id_empresa AND $item = :$item ORDER BY `nombre` ASC");

		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	EDITAR Persona
	=============================================*/

	static public function mdlEditarPersona($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("
			UPDATE $tabla
			SET cedula 				= :cedula,
				nombre 				= :nombre,
				apellido 			= :apellido,
				apodo 				= :apodo,
				fecha_nacimiento 	= :fecha_nacimiento,
				telefono 			= :telefono,
				email 				= :email,
				sexo 				= :sexo,
				direccion 			= :direccion,
				ocupacion 			= :ocupacion,
				tel_trabajo 		= :tel_trabajo,
				documento 			= :documento,
				observacion 		= :observacion
			WHERE
				id_empresa = :id_empresa
			AND id_persona = :id_persona
		");

		$stmt->bindParam(":cedula", 			$datos['PerCedula'], PDO::PARAM_STR);
		$stmt->bindParam(":nombre", 			$datos['PerNombre'], PDO::PARAM_STR);
		$stmt->bindParam(":apellido", 			$datos['PerApellido'], PDO::PARAM_STR);
		$stmt->bindParam(":apodo", 				$datos['apodo'], PDO::PARAM_STR);
		$stmt->bindParam(":fecha_nacimiento", 	$datos['PerFecha_nacimiento'], PDO::PARAM_STR);
		$stmt->bindParam(":telefono", 			$datos['PerTelefono'], PDO::PARAM_STR);
		$stmt->bindParam(":email", 				$datos['PerEmail'], PDO::PARAM_STR);
		$stmt->bindParam(":sexo", 				$datos['PerSexo'], PDO::PARAM_STR);
		$stmt->bindParam(":direccion", 			$datos['PerDireccion'], PDO::PARAM_STR);
		$stmt->bindParam(":ocupacion", 			$datos['ocupacion'], PDO::PARAM_STR);
		$stmt->bindParam(":tel_trabajo", 		$datos['tel_trabajo'], PDO::PARAM_STR);
		$stmt->bindParam(":documento", 			$datos['documento'], PDO::PARAM_STR);
		$stmt->bindParam(":observacion", 		$datos['observacion'], PDO::PARAM_STR);
		$stmt->bindParam(":id_empresa", 		$datos['IdEmpresa'], PDO::PARAM_INT);
		$stmt->bindParam(":id_persona", 		$datos['id_persona'], PDO::PARAM_INT);


		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	static public function mdlEditarImagen($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET imagen = :imagen WHERE id_persona = :id_persona");

		$stmt->bindParam(":imagen", $datos['imagen'], PDO::PARAM_STR);
		$stmt->bindParam(":id_persona", $datos['id_persona'], PDO::PARAM_INT);


		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	BORRAR Persona
	=============================================*/

	static public function mdlBorrarPersona($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_persona = :id_persona ");

		$stmt -> bindParam(":id_persona", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR Documentos de Persona
	=============================================*/
	static public function mdlMostrarDocumentos($tabla, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ");

		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	Isertar Documentos temp
	=============================================*/

	static public function mdlDocumentosTemp($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id_persona, name) VALUES (:id_persona, :name)");

		$stmt->bindParam(":id_persona", $datos['id_persona'], PDO::PARAM_STR);
		$stmt->bindParam(":name", $datos['name'], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	Isertar Documentos
	=============================================*/

	static public function mdlAddDocumentos($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id_persona, nombre_documento, descripcion) VALUES (:id_persona, :nombre_documento, :descripcion)");

		$stmt->bindParam(":id_persona", 		$datos['id_persona'], PDO::PARAM_INT);
		$stmt->bindParam(":nombre_documento", 	$datos['nombre_documento'], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", 		$datos['descripcion'], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ELIMINAR Documentos Temp
	=============================================*/

	static public function mdlDelTempDocumentos($tabla,$item, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE $item = :id_user");

		$stmt -> bindParam(":id_user", $datos, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}



}