<?php

require_once "conexion.php";

class ModeloEvidencias{

	/*=============================================
	CREAR Evidencias
	=============================================*/

	static public function mdlRegistroEvidencias($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("

			INSERT INTO $tabla ( Descevidencia, Foto)
			VALUES
				(
					:Descevidencia, :Foto
				)
			");

		$stmt->bindParam(":Descevidencia", 		$datos['Descevidencia'], PDO::PARAM_STR);
		$stmt->bindParam(":Foto", 				$datos['Foto'], PDO::PARAM_STR);
		

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Editar Evidencias
	=============================================*/

	static public function mdlEditarEvidencias($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("UPDATE $tabla SET Descevidencia = :Descevidencia, Foto = :Foto WHERE Idevidencia = :Idevidencia ");

		$stmt->bindParam(":Descevidencia", 		$datos['Descevidencia'], PDO::PARAM_STR);
		$stmt->bindParam(":Foto", 				$datos['Foto'], PDO::PARAM_STR);
		$stmt->bindParam(":Idevidencia", 		$datos['Idevidencia'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Elimina Evidencias
	=============================================*/

	static public function mdlEliminarEvidencias($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("DELETE FROM $tabla WHERE Idevidencia = :Idevidencia ");

		$stmt->bindParam(":Idevidencia", 		$datos['Idevidencia'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR
	=============================================*/

	static public function mdlMostrarEvidencias($tabla){

		$stmt = Conexion::conectar()->prepare(" SELECT * FROM $tabla ");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}
}
