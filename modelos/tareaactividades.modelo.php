<?php

require_once "conexion.php";

class Modelotareaactividad{

	/*=============================================
	CREAR tarea
	=============================================*/

	static public function mdlRegistrotarea($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("

			INSERT INTO $tabla (Idactividades, DescTarea)
			VALUES
				(
					 :Idactividades, :DescTarea
				)
			");

		$stmt->bindParam(":Idactividades", 				$datos['Idactividades'], PDO::PARAM_INT);
		$stmt->bindParam(":DescTarea", 					$datos['DescTarea'], PDO::PARAM_STR);
		

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Editar Ejes
	=============================================*/

	static public function mdlEditartarea($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("UPDATE $tabla SET NomTarea = :NomTarea, DescTarea = :DescTarea, EstadoTarea = :EstadoTarea WHERE Idtareaactividades = :Idtareaactividades ");

		$stmt->bindParam(":NomTarea", 					$datos['NomTarea'], PDO::PARAM_STR);
		$stmt->bindParam(":DescTarea", 					$datos['DescTarea'], PDO::PARAM_STR);
		$stmt->bindParam(":EstadoTarea", 				$datos['EstadoTarea'], PDO::PARAM_STR);
		$stmt->bindParam(":Idtareaactividades", 		$datos['Idtareaactividades'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Elimina Ejes
	=============================================*/

	static public function mdlEliminartarea($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("DELETE FROM $tabla WHERE Idtareaactividades = :Idtareaactividades ");

		$stmt->bindParam(":Idtareaactividades", 		$datos['Idtareaactividades'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR
	=============================================*/
	static public function mdlMostrarActividad($tabla,$Idactividades){

		$stmt = Conexion::conectar()->prepare(" SELECT * FROM $tabla WHERE Idactividades = :Idactividades ");

		$stmt->bindParam(":Idactividades", 		$Idactividades, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;

	}

	static public function mdlMostrartarea($tabla,$Idactividades){

		$stmt = Conexion::conectar()->prepare(" SELECT * FROM $tabla WHERE Idactividades = :Idactividades ");

		$stmt->bindParam(":Idactividades", 		$Idactividades, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}

	/*/ ////////////////////////////////////////////////////////
	Evidencias
	////////////////////////////////////////////////////////////*/
	
	static public function mdlMostrarImgTemp($tabla, $item, $valor){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();

			$stmt -> close();

			$stmt = null;

	}

	/*=============================================
	Isertar Img temp
	=============================================*/

	static public function mdlImgTemp($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id_user, name) VALUES (:id_user, :name) ");

		$stmt->bindParam(":id_user", 	$datos['id_user'], PDO::PARAM_STR);
		$stmt->bindParam(":name", 		$datos['name'], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	Isertar Img
	=============================================*/

	static public function mdlInsertImg($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id_detalle, foto) VALUES (:id_detalle, :foto)");

		$stmt->bindParam(":id_detalle", $datos['id_detalle'], PDO::PARAM_INT);
		$stmt->bindParam(":foto", 		$datos['foto'], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}


	static public function mdlRegistrarEvidencia($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("INSERT INTO $tabla (Idtareaactividades, accion, estado, progreso) VALUES (:Idtareaactividades, :accion, :estado, :progreso) ");

		$stmt->bindParam(":Idtareaactividades", 	$datos['Idtareaactividades'], PDO::PARAM_INT);
		$stmt->bindParam(":accion", 				$datos['accion'], PDO::PARAM_STR);
		$stmt->bindParam(":estado", 				$datos['estado'], PDO::PARAM_STR);
		$stmt->bindParam(":progreso", 				$datos['progreso'], PDO::PARAM_STR);

		if($stmt->execute()){

			return $lastId = $pdo->lastInsertId();

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}


	/*=============================================
	ELIMINAR Img Temp
	=============================================*/

	static public function mdlDelTempImg($tabla,$item, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE $item = :id_user");

		$stmt -> bindParam(":id_user", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	Motrar detalle de tarea
	=============================================*/
	static public function mdlMostrarTareaDetalle($tabla,$Idactividades){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE Idtareaactividades = :Idtareaactividades ");

			$stmt -> bindParam(":Idtareaactividades", $Idactividades, PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetchAll();

			$stmt -> close();

			$stmt = null;

	}

	/*=============================================
	Motrar evidencia
	=============================================*/
	static public function mdlMostrarEvidencia($tabla,$IdDetalle){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_detalle = :id_detalle ");

			$stmt -> bindParam(":id_detalle", $IdDetalle, PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetchAll();

			$stmt -> close();

			$stmt = null;

	}
}
