<?php

require_once "conexion.php";

class ModeloDirectores{

	/*=============================================
	CREAR Directores
	=============================================*/

	static public function mdlRegistroDirectores($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("

			INSERT INTO $tabla ( IdDirector, NomDirector, Idusuario, Correo, NomDepartamento, Estado)
			VALUES
				(
					:IdDirector,
					:NomDirector,
					:Idusuario,
					:Correo,
					:NomDepartamento,
					:Estado
				)
			");

		$stmt->bindParam(":IdDirector", 		$datos['IdDirector'], PDO::PARAM_INT);
		$stmt->bindParam(":NomDirector", 		$datos['NomDirector'], PDO::PARAM_STR);
		$stmt->bindParam(":Idusuario", 			$datos['Idusuario'], PDO::PARAM_INT);
		$stmt->bindParam(":Correo", 			$datos['Correo'], PDO::PARAM_STR);
		$stmt->bindParam(":NomDepartamento", 	$datos['NomDepartamento'], PDO::PARAM_STR);
		$stmt->bindParam(":Estado", 			$datos['Estado'], PDO::PARAM_STR);
		

		if($stmt->execute()){

			return $lastId = $pdo->lastInsertId();

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}
	/*=============================================
	Editar Ejes
	=============================================*/

	static public function mdlEditarDirectores($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("UPDATE $tabla SET Correo = :Correo, NomDepartamento = :NomDepartamento, Estado = :Estado, Idusuario = :Idusuario, NomDirector = :NomDirector WHERE IdDirector = :IdDirector ");

		$stmt->bindParam(":Idusuario", 			$datos['Idusuario'], PDO::PARAM_STR);
		$stmt->bindParam(":NomDirector", 		$datos['NomDirector'], PDO::PARAM_STR);
		$stmt->bindParam(":IdDirector", 		$datos['IdDirector'], PDO::PARAM_INT);
		$stmt->bindParam(":Correo", 			$datos['Correo'], PDO::PARAM_STR);
		$stmt->bindParam(":NomDepartamento", 	$datos['NomDepartamento'], PDO::PARAM_STR);
		$stmt->bindParam(":Estado", 			$datos['Estado'], PDO::PARAM_STR);


		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	Elimina Ejes
	=============================================*/

	static public function mdlEliminarDirectores($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("DELETE FROM $tabla WHERE IdDirector = :IdDirector ");

		$stmt->bindParam(":IdDirector", 		$datos['IdDirector'], PDO::PARAM_INT);

		if($stmt->execute()){

			//return $lastId = $pdo->lastInsertId();
			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR
	=============================================*/

	static public function mdlMostrarDirectores($tabla){

		$stmt = Conexion::conectar()->prepare(" SELECT * FROM $tabla ");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}
}


