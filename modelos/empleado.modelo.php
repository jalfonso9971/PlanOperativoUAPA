<?php

require_once "conexion.php";

class ModeloEmpleado{

	/*=============================================
	CREAR Empleado
	=============================================*/

	static public function mdlRegistroEmpleado($tabla, $datos){

		$pdo = Conexion::conectar();

		$stmt = $pdo->prepare("INSERT INTO $tabla (id_persona,id_roles,estado) VALUES (:id_persona,:id_roles,:estado)");

		$stmt->bindParam(":id_persona", $datos['id_persona'], PDO::PARAM_INT);
		$stmt->bindParam(":id_roles", $datos['id_roles'], PDO::PARAM_INT);
		$stmt->bindParam(":estado", $datos['estado'], PDO::PARAM_INT);

		if($stmt->execute()){

			return $lastId = $pdo->lastInsertId();

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR Empleado
	=============================================*/

	static public function mdlMostrarEmpleado($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE estado_category = 1");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	EDITAR Empleado
	=============================================*/

	static public function mdlEditarEmpleado($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET Empleado = :Empleado WHERE id = :id");

		$stmt -> bindParam(":Empleado", $datos["Empleado"], PDO::PARAM_STR);
		$stmt -> bindParam(":id", $datos["id"], PDO::PARAM_INT);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	BORRAR Empleado
	=============================================*/

	static public function mdlBorrarEmpleado($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

}