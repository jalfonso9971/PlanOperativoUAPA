<?php

class ControladorDepartamento{

	/*=============================================
	CREAR credit_cardS
	=============================================*/

	static public function ctrCrearDepartamento(){


		if(isset($_POST["add_departamento"])){

			$tabla = "departamentos";

			$datos = array(							
							'NomDeparta' 		=> $_POST["NomDeparta"],
							'DescDeparta' 		=> $_POST["DescDeparta"],
							'Estado' 			=> $_POST['Estado']
						);
				
			$respuesta = Modelodepartamento::mdlRegistroDepartamento($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo el departamento '.$_POST['NomDeparta']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}

	}


	/*=============================================
	MOSTRAR credit_cardS
	=============================================*/

	static public function ctrMostrarDepartamento(){

		$tabla = "departamentos";

		$respuesta = Modelodepartamento::mdlMostrarDepartamento($tabla);

		return $respuesta;
	}

	/*=============================================
	MOSTRAR Detalle de Persona
	=============================================*/

	static public function ctrEditardepartamentos(){

		if(isset($_POST["edit_departamentos"])){

			$tabla = "departamentos";

			$datos = array(	'NomDeparta' 		=> $_POST["NomDeparta"],
							'DescDeparta' 		=> $_POST['DescDeparta'],
							'Estado' 			=> $_POST['Estado'],
							'IdDeparta' 		=> $_POST['IdDeparta']
						);
				
			$respuesta = Modelodepartamentos::mdlEditarDepartamento($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo el departamento '.$_POST['NomDeparta']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}

	/*=============================================
	Eliminar
	=============================================*/

	static public function ctrEliminardepartamentos(){

		if(isset($_POST["eliminar_departamentos"])){

			$tabla = "departamentos";

			$datos = array(
							'IdDeparta' 		=> $_POST['IdDeparta']
						);
				
			$respuesta = Modelodepartamento::mdlEliminarDepartamento($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' eliminio el departamento '.$_POST['IdDeparta']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos eliminado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al eliminar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}
}
?>