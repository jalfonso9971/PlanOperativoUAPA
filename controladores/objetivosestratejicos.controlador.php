<?php

class Controladorobjetivosestratejicos{

	/*=============================================
	CREAR credit_cardS
	=============================================*/

	static public function ctrCrearobjetivosestratejicos(){


		if(isset($_POST["add_objetivosestratejicos"])){

			$tabla = "obj_estratejicos";

			$datos = array(	'nomobjestratejico' 		=> $_POST["nomobjestratejico"],
							'descobjestratejico' 		=> $_POST['descobjestratejico'],
							'IdEjes' 					=> $_POST['IdEje']
						);
				
			$respuesta = ModeloObjetivosEstratejicos::mdlRegistroObjetivoEstratejico($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo el objetivo '.$_POST['nomobjestratejico']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}

	}


	/*=============================================
	MOSTRAR credit_cardS
	=============================================*/

	static public function ctrMostrarobjetivosestratejicos($IdEje){

		$tabla = "obj_estratejicos";

		$respuesta = ModeloObjetivosEstratejicos::mdlMostrarobjetivosestratejicos($tabla,$IdEje);

		return $respuesta;
	}

	static public function ctrMostrarobjetivosestratejicosDashboard(){


		$respuesta = ModeloObjetivosEstratejicos::mdlMostrarobjetivosestratejicosDashboard();

		return $respuesta;
	}

	/*=============================================
	MOSTRAR Detalle de Persona
	=============================================*/

	static public function ctrEditarobjetivosestratejicos(){

		if(isset($_POST["edit_obj_estratejicos"])){

			$tabla = "obj_estratejicos";

			$datos = array(	'nomobjestratejico' 		=> $_POST["nomobjestratejico"],
							'descobjestratejico' 		=> $_POST['descobjestratejico'],
							'Idobjestratejico' 			=> $_POST['Idobjestratejico']
						);
				
			$respuesta = ModeloObjetivosEstratejicos::mdlEditarobjetivosestratejicos($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo el objetivo '.$_POST['nomobjestratejico']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}

	/*=============================================
	Eliminar
	=============================================*/

	static public function ctrEliminarobjetivosestratejicos(){

		if(isset($_POST["eliminar_obj_estratejicos"])){

			$tabla = "obj_estratejicos";

			$datos = array(
							'Idobjestratejico' 		=> $_POST['Idobjestratejico']
						);
				
			$respuesta = ModeloObjetivosEstratejicos::mdlEliminarobjetivosestratejicos($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' eliminio el objetivo '.$_POST['Idobjestratejico']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos eliminado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al eliminar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}
}
?>