<?php



class Controladoractividades{

	/*=============================================
	CREAR credit_cardS
	=============================================*/

	static public function ctrCrearactividades(){
// print_r($_POST);
		if(isset($_POST["add_actividades"])){

			
				$tabla = "actividad";

				$datos = array(	
								'Nomactividades' 			=> $_POST['Nomactividades'],
								'descactividades' 			=> $_POST['descactividades'],
								'fechainicio' 				=> $_POST['fechainicio'],
								'fechafin' 					=> $_POST['fechafin'],
								'Idmeta' 					=> $_POST['Idmetas']
							);
								
				$respuesta = Modeloactividades::mdlRegistroactividades($tabla, $datos);

				if($respuesta == 'ok'){

					ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo la actividad '.$_POST['Nomactividades'].' '.$_POST['fechainicio'].' fechafin '.$_POST['fechafin']);

					echo'<script>

					Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}

	}


/*=============================================
	MOSTRAR credit_cardS
	=============================================*/

	static public function ctrMostraractividades($Idmetas){

		$tabla = "actividad";

		$respuesta = Modeloactividades::mdlMostraractividades($tabla, $Idmetas);

		return $respuesta;
	}

	static public function ctrMostraractividadesDashboard(){
		

		$respuesta = Modeloactividades::mdlMostraractividadesDashboard();

		return $respuesta;
	}

	/*=============================================
	MOSTRAR Detalle de actividades
	=============================================*/

	static public function ctrEditaractividades(){

		if(isset($_POST["edit_actividades"])){

			$tabla = "actividad";

			$datos = array(	'Idactividades' 		=> $_POST["Idactividades"],
							'Nomactividades' 		=> $_POST['Nomactividades'],
							'descactividades' 		=> $_POST['descactividades'],
							'fechainicio' 			=> $_POST["fechainicio"],
							'fechafin' 				=> $_POST['fechafin']
						);
				
			$respuesta = Modeloactividades::mdlEditaractividades($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo el actividad '.$_POST['Nomactividades']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos registrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al registrar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}

	/*=============================================
	Eliminar
	=============================================*/

	static public function ctrEliminaractividades(){

		if(isset($_POST["eliminar_actividades"])){

			$tabla = "actividad";

			$datos = array(
							'Idactividades' 		=> $_POST['Idactividades']
						);
				
			$respuesta = Modeloactividades::mdlEliminaractividades($tabla, $datos);

			if($respuesta == 'ok'){

				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' eliminio el actividad '.$_POST['Idactividades']);

				echo'<script>

				Swal.fire({
					  type: "success",
					  title: "Datos eliminado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

								}
							})

				</script>';

			}else{

			echo'<script>

				Swal.fire({
					  type: "error",
					  title: "¡ERROR al eliminar los datos!",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
						if (result.value) {

						window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

						}
					})

		  	</script>';

			}

		}
	}
}
?>