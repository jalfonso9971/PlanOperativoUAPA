<?php
class ControladorUsuarios{

	/*=============================================
	INGRESO DE USUARIO
	=============================================*/

	static public function ctrIngresoUsuario(){

		if(isset($_POST["ingUsuario"])){

			if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingUsuario"])){

			   	$encriptar = crypt($_POST["ingPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				$tabla = "usuario";

				$item = "Usuario";
				$valor = $_POST["ingUsuario"];

				$respuesta = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item, $valor);

				if($respuesta["Usuario"] == $_POST["ingUsuario"] && $respuesta["password"] == $encriptar){

					if($respuesta["estado"] == 1){

						$_SESSION["iniciarSesion"] 	= "ok";
						$_SESSION["id_user"] 		= $respuesta["Idusuario"];
						$_SESSION["nombre"] 		= $respuesta["Nombre"];
						$_SESSION["usuario"] 		= $respuesta["Usuario"];
						$_SESSION["email"] 			= $respuesta["email"];
						$_SESSION["rol"] 			= $respuesta["Rol"];


						if($_SESSION["iniciarSesion"] == "ok"){

							echo '<script>

									window.location = "home";
								</script>';

						}

					}else{

						echo '<br>
							<div class="alert alert-danger">El usuario aún no está activado.</div>';

					}

				}else{

					echo '<br><div class="alert alert-danger">Error al entrar, inténtalo de nuevo</div>';

				}

			}

		}

	}

	/*=============================================
	REGISTRO DE USUARIO
	=============================================*/

	static public function ctrCrearUsuario($datos){

		if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $datos["LogUsuario"]) &&
			   preg_match('/^[a-zA-Z0-9]+$/', $datos["LogUsuario"]) &&
			   preg_match('/^[a-zA-Z0-9]+$/', $datos["LogContrasena"]) && $datos["LogContrasena"] === $datos["LogConfirContrasena"]){

			   	/*=============================================
				VALIDAR IMAGEN https://sweetalert2.github.io/#download
				=============================================*/
				$tabla 		= "usuario";

				$item 		= "usuario";
				$valor 		= $datos["LogUsuario"];

				$respuesta 	= ModeloUsuarios::mdlMostrarUsuariosRegistro($tabla, $item, $valor);

					if ($respuesta["total"] > 0) {

						echo '<script>

							Swal.fire({

								type: "error",
								title: "El usuario no puede ir vacío o llevar caracteres especiales!",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){

									window.location = "register";

								}

							});


						</script>';
					} else {
						//---------------------------------------
						//Bitacora
						//---------------------------------------
						ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' creo el usuario '.$datos["LogUsuario"]);

						$tabla 		= "usuario";

						$encriptar 	= crypt($datos["LogContrasena"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

						$datos 		= array("id_empleado" 	=> $datos["id_empleado"],
							           		"LogUsuario" 	=> $datos["LogUsuario"],
							           		"password" 		=> $encriptar);

						return $respuesta = ModeloUsuarios::mdlIngresarUsuario($tabla, $datos);

						/*
						if($respuesta == "ok"){

							echo '<script>

							Swal.fire({

								type: "success",
								title: "El usuario se ha guardado correctamente.!",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){

									window.location = "login";

								}

							});


							</script>';


						}*/
					}


				}else{

					echo '<script>

						Swal.fire({

							type: "error",
							title: "El usuario no puede ir vacío o llevar caracteres especiales.!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"

						}).then(function(result){

							if(result.value){

								window.location = "register";

							}

						});


					</script>';

				}


	}

	/*=============================================
	MOSTRAR USUARIO
	=============================================*/

	static public function ctrMostrarUsuarios($item, $valor){

		$tabla = "usuario, empleado, persona";

		$respuesta = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item, $valor);

		return $respuesta;
	}


	static public function ctrMostrarUsuariosProfile($valor){

		$respuesta = ModeloUsuarios::mdlMostrarUsuariosProfile($valor);

		return $respuesta;
	}

	static public function ctrMostrarUsuariosRegistro($item, $valor){

		$tabla = "usuario";

		$respuesta = ModeloUsuarios::mdlMostrarUsuariosRegistro($tabla, $item, $valor);

		return $respuesta;
	}

	static public function ctrMostrarUsuariosEmp($valor){

		$respuesta = ModeloUsuarios::mdlMostrarUsuariosEmp($valor);

		return $respuesta;
	}

	/*=============================================
	EDITAR USUARIO
	=============================================*/

	static public function ctrEditarUsuario(){

		if(isset($_POST["editarUsuario"])){

				//entritamos contraseña
				$editarPasswordActual = crypt($_POST["LogContrasena"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				if($_POST["LogContrasena"] != ""){

					if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["LogConfirContrasena"])){

						$encriptar = crypt($_POST["LogConfirContrasena"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

					}else{

						echo'<script>

								Swal.fire({
									  type: "error",
									  title: "¡La contraseña no puede ir vacía!",
									  showConfirmButton: true,
									  confirmButtonText: "Cerrar"
									  }).then(function(result) {
										if (result.value) {

										window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

										}
									})

						  	</script>';

						  	return;

					}

				}else{

					$encriptar = $_POST["passwordActual"];

				}

				$datos = array("id_roles" 	=> $_POST["IdCargo"],
							   "contrasena" => $encriptar,
							   "id_usuario" => $_POST["id_usuario"],
							   "estado" 	=> $_POST["estado"]);

				$respuesta = ModeloUsuarios::mdlEditarUsuario($datos);

				if($respuesta == "ok"){

					//---------------------------------------
					//Bitacora
					//---------------------------------------
					ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' actualizo el usuario '.$_POST["editarUsuario"]);

					echo'<script>

					Swal.fire({
						  type: "success",
						  title: "El usuario ha sido editado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result) {
									if (result.value) {

									window.location = "https://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'";

									}
								})

					</script>';

				}


		}

	}

	/*=============================================
	BORRAR USUARIO
	=============================================*/

	static public function ctrBorrarUsuario(){

		if(isset($_GET["idUsuario"])){

			$tabla 					= "usuarios";
			$datos 					= $_GET["idUsuario"];

			if($_GET["fotoUsuario"] != ""){

				unlink($_GET["fotoUsuario"]);
				rmdir('vistas/img/usuarios/'.$_GET["usuario"]);

			}

			$respuesta 				= ModeloUsuarios::mdlBorrarUsuario($tabla, $datos);

			if($respuesta == "ok"){

				//---------------------------------------
				//Bitacora
				//---------------------------------------
				ControladorBitacora::ctrCrearBitacora('El usuario '.$_SESSION["usuario"].' Elimino el usuario ID: '.$_GET["idUsuario"]);

				echo'<script>

				swal({
					  type: "success",
					  title: "El usuario ha sido borrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar",
					  closeOnConfirm: false
					  }).then(function(result) {
								if (result.value) {

								window.location = "usuarios";

								}
							})

				</script>';

			}

		}

	}


}