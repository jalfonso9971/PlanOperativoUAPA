-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-03-2020 a las 05:46:46
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `plan_operativo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `Idactividades` int(11) NOT NULL,
  `Idmeta` int(11) NOT NULL,
  `Nomactividades` varchar(30) NOT NULL,
  `descactividades` varchar(300) NOT NULL,
  `fechainicio` date NOT NULL,
  `fechafin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `actividad`
--

INSERT INTO `actividad` (`Idactividades`, `Idmeta`, `Nomactividades`, `descactividades`, `fechainicio`, `fechafin`) VALUES
(2, 2, '1', 'Consultar a participantes egresados, facilitadores y directores.', '2020-03-02', '2020-03-25'),
(6, 3, '2', 'Crear la comisión revisora.\r\n\r\n', '2020-03-14', '2020-03-21'),
(11, 6, '1', 'Socializar con el Dpto. de Evaluación de los Aprendizajes los criterios del protocolo.', '2020-03-09', '2020-03-31'),
(14, 2, '2', 'Elaborar Informe técnico.', '2020-03-20', '2020-03-27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `id_bitacora` int(15) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  `accion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bitacora`
--

INSERT INTO `bitacora` (`id_bitacora`, `id_usuario`, `fecha`, `accion`) VALUES
(1, 2, '2020-03-07 03:36:26', 'El usuario admin creo el eje aaa'),
(2, 2, '2020-03-07 03:37:04', 'El usuario admin creo el eje aaa'),
(3, 2, '2020-03-07 03:39:50', 'El usuario admin creo el eje aaa'),
(4, 2, '2020-03-07 03:40:04', 'El usuario admin creo el eje aaa'),
(5, 2, '2020-03-07 03:40:23', 'El usuario admin creo el eje aaa'),
(6, 2, '2020-03-07 03:40:57', 'El usuario admin creo el eje aaa'),
(7, 2, '2020-03-07 03:42:05', 'El usuario admin creo el eje aaa'),
(8, 2, '2020-03-07 03:42:11', 'El usuario admin creo el eje aaa'),
(9, 2, '2020-03-07 03:42:22', 'El usuario admin creo el eje aaa'),
(10, 2, '2020-03-07 03:42:35', 'El usuario admin creo el eje aww'),
(11, 2, '2020-03-07 03:43:36', 'El usuario admin creo el eje aww'),
(12, 2, '2020-03-07 03:45:05', 'El usuario admin creo el eje rtt'),
(13, 2, '2020-03-07 03:59:33', 'El usuario admin creo el eje Jose'),
(14, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(15, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(16, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(17, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(18, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(19, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(20, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(21, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(22, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(23, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(24, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(25, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(26, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(27, 2, '2020-03-07 04:10:58', 'El usuario admin creo el eje aaa'),
(28, 2, '2020-03-07 04:11:09', 'El usuario admin creo el eje aaaaaeee'),
(29, 2, '2020-03-07 04:11:09', 'El usuario admin creo el eje aaaaaeee'),
(30, 2, '2020-03-07 04:11:09', 'El usuario admin creo el eje aaaaaeee'),
(31, 2, '2020-03-07 04:11:09', 'El usuario admin creo el eje aaaaaeee'),
(32, 2, '2020-03-07 04:11:09', 'El usuario admin creo el eje aaaaaeee'),
(33, 2, '2020-03-07 04:11:09', 'El usuario admin creo el eje aaaaaeee'),
(34, 2, '2020-03-07 04:11:09', 'El usuario admin creo el eje aaaaaeee'),
(35, 2, '2020-03-07 04:11:09', 'El usuario admin creo el eje aaaaaeee'),
(36, 2, '2020-03-07 04:11:09', 'El usuario admin creo el eje aaaaaeee'),
(37, 2, '2020-03-07 04:11:10', 'El usuario admin creo el eje aaaaaeee'),
(38, 2, '2020-03-07 04:11:10', 'El usuario admin creo el eje aaaaaeee'),
(39, 2, '2020-03-07 04:11:10', 'El usuario admin creo el eje aaaaaeee'),
(40, 2, '2020-03-07 04:11:10', 'El usuario admin creo el eje aaaaaeee'),
(41, 2, '2020-03-07 04:11:10', 'El usuario admin creo el eje aaaaaeee'),
(42, 2, '2020-03-07 04:16:41', 'El usuario admin eliminio el eje 1'),
(43, 2, '2020-03-07 04:16:41', 'El usuario admin eliminio el eje 1'),
(44, 2, '2020-03-07 04:16:41', 'El usuario admin eliminio el eje 1'),
(45, 2, '2020-03-07 04:16:42', 'El usuario admin eliminio el eje 1'),
(46, 2, '2020-03-07 04:16:42', 'El usuario admin eliminio el eje 1'),
(47, 2, '2020-03-07 04:16:42', 'El usuario admin eliminio el eje 1'),
(48, 2, '2020-03-07 04:16:42', 'El usuario admin eliminio el eje 1'),
(49, 2, '2020-03-07 04:16:42', 'El usuario admin eliminio el eje 1'),
(50, 2, '2020-03-07 04:16:42', 'El usuario admin eliminio el eje 1'),
(51, 2, '2020-03-07 04:16:42', 'El usuario admin eliminio el eje 1'),
(52, 2, '2020-03-07 04:16:42', 'El usuario admin eliminio el eje 1'),
(53, 2, '2020-03-07 04:16:42', 'El usuario admin eliminio el eje 1'),
(54, 2, '2020-03-07 04:16:42', 'El usuario admin eliminio el eje 1'),
(55, 2, '2020-03-07 04:16:42', 'El usuario admin eliminio el eje 1'),
(56, 2, '2020-03-07 04:17:27', 'El usuario admin eliminio el eje 2'),
(57, 2, '2020-03-07 04:17:27', 'El usuario admin eliminio el eje 2'),
(58, 2, '2020-03-07 04:17:27', 'El usuario admin eliminio el eje 2'),
(59, 2, '2020-03-07 04:17:28', 'El usuario admin eliminio el eje 2'),
(60, 2, '2020-03-07 04:17:28', 'El usuario admin eliminio el eje 2'),
(61, 2, '2020-03-07 04:17:28', 'El usuario admin eliminio el eje 2'),
(62, 2, '2020-03-07 04:17:28', 'El usuario admin eliminio el eje 2'),
(63, 2, '2020-03-07 04:17:28', 'El usuario admin eliminio el eje 2'),
(64, 2, '2020-03-07 04:17:28', 'El usuario admin eliminio el eje 2'),
(65, 2, '2020-03-07 04:17:28', 'El usuario admin eliminio el eje 2'),
(66, 2, '2020-03-07 04:17:28', 'El usuario admin eliminio el eje 2'),
(67, 2, '2020-03-07 04:17:28', 'El usuario admin eliminio el eje 2'),
(68, 2, '2020-03-07 04:17:28', 'El usuario admin eliminio el eje 2'),
(69, 2, '2020-03-07 04:17:40', 'El usuario admin eliminio el eje 3'),
(70, 2, '2020-03-07 04:17:40', 'El usuario admin eliminio el eje 3'),
(71, 2, '2020-03-07 04:17:40', 'El usuario admin eliminio el eje 3'),
(72, 2, '2020-03-07 04:17:41', 'El usuario admin eliminio el eje 3'),
(73, 2, '2020-03-07 04:17:41', 'El usuario admin eliminio el eje 3'),
(74, 2, '2020-03-07 04:17:41', 'El usuario admin eliminio el eje 3'),
(75, 2, '2020-03-07 04:17:41', 'El usuario admin eliminio el eje 3'),
(76, 2, '2020-03-07 04:17:41', 'El usuario admin eliminio el eje 3'),
(77, 2, '2020-03-07 04:17:41', 'El usuario admin eliminio el eje 3'),
(78, 2, '2020-03-07 04:17:41', 'El usuario admin eliminio el eje 3'),
(79, 2, '2020-03-07 04:17:41', 'El usuario admin eliminio el eje 3'),
(80, 2, '2020-03-07 04:17:41', 'El usuario admin eliminio el eje 3'),
(81, 2, '2020-03-07 04:19:09', 'El usuario admin eliminio el eje 4'),
(82, 2, '2020-03-07 04:19:09', 'El usuario admin eliminio el eje 4'),
(83, 2, '2020-03-07 04:19:09', 'El usuario admin eliminio el eje 4'),
(84, 2, '2020-03-07 04:19:09', 'El usuario admin eliminio el eje 4'),
(85, 2, '2020-03-07 04:19:09', 'El usuario admin eliminio el eje 4'),
(86, 2, '2020-03-07 04:19:09', 'El usuario admin eliminio el eje 4'),
(87, 2, '2020-03-07 04:19:09', 'El usuario admin eliminio el eje 4'),
(88, 2, '2020-03-07 04:19:09', 'El usuario admin eliminio el eje 4'),
(89, 2, '2020-03-07 04:19:09', 'El usuario admin eliminio el eje 4'),
(90, 2, '2020-03-07 04:19:10', 'El usuario admin eliminio el eje 4'),
(91, 2, '2020-03-07 04:19:10', 'El usuario admin eliminio el eje 4'),
(92, 2, '2020-03-07 04:19:55', 'El usuario admin eliminio el eje 5'),
(93, 2, '2020-03-07 04:19:55', 'El usuario admin eliminio el eje 5'),
(94, 2, '2020-03-07 04:19:55', 'El usuario admin eliminio el eje 5'),
(95, 2, '2020-03-07 04:19:55', 'El usuario admin eliminio el eje 5'),
(96, 2, '2020-03-07 04:19:55', 'El usuario admin eliminio el eje 5'),
(97, 2, '2020-03-07 04:19:55', 'El usuario admin eliminio el eje 5'),
(98, 2, '2020-03-07 04:19:55', 'El usuario admin eliminio el eje 5'),
(99, 2, '2020-03-07 04:19:55', 'El usuario admin eliminio el eje 5'),
(100, 2, '2020-03-07 04:19:55', 'El usuario admin eliminio el eje 5'),
(101, 2, '2020-03-07 04:19:55', 'El usuario admin eliminio el eje 5'),
(102, 2, '2020-03-07 04:20:27', 'El usuario admin eliminio el eje 5'),
(103, 2, '2020-03-07 04:20:27', 'El usuario admin eliminio el eje 5'),
(104, 2, '2020-03-07 04:20:27', 'El usuario admin eliminio el eje 5'),
(105, 2, '2020-03-07 04:20:27', 'El usuario admin eliminio el eje 5'),
(106, 2, '2020-03-07 04:20:27', 'El usuario admin eliminio el eje 5'),
(107, 2, '2020-03-07 04:20:27', 'El usuario admin eliminio el eje 5'),
(108, 2, '2020-03-07 04:20:27', 'El usuario admin eliminio el eje 5'),
(109, 2, '2020-03-07 04:20:27', 'El usuario admin eliminio el eje 5'),
(110, 2, '2020-03-07 04:20:27', 'El usuario admin eliminio el eje 5'),
(111, 2, '2020-03-07 04:20:32', 'El usuario admin eliminio el eje 6'),
(112, 2, '2020-03-07 04:20:33', 'El usuario admin eliminio el eje 6'),
(113, 2, '2020-03-07 04:20:33', 'El usuario admin eliminio el eje 6'),
(114, 2, '2020-03-07 04:20:33', 'El usuario admin eliminio el eje 6'),
(115, 2, '2020-03-07 04:20:33', 'El usuario admin eliminio el eje 6'),
(116, 2, '2020-03-07 04:20:33', 'El usuario admin eliminio el eje 6'),
(117, 2, '2020-03-07 04:20:33', 'El usuario admin eliminio el eje 6'),
(118, 2, '2020-03-07 04:20:33', 'El usuario admin eliminio el eje 6'),
(119, 2, '2020-03-07 04:20:33', 'El usuario admin eliminio el eje 6'),
(120, 2, '2020-03-07 04:24:02', 'El usuario admin eliminio el eje 7'),
(121, 2, '2020-03-07 04:24:02', 'El usuario admin eliminio el eje 7'),
(122, 2, '2020-03-07 04:24:02', 'El usuario admin eliminio el eje 7'),
(123, 2, '2020-03-07 04:24:02', 'El usuario admin eliminio el eje 7'),
(124, 2, '2020-03-07 04:24:02', 'El usuario admin eliminio el eje 7'),
(125, 2, '2020-03-07 04:24:02', 'El usuario admin eliminio el eje 7'),
(126, 2, '2020-03-07 04:24:02', 'El usuario admin eliminio el eje 7'),
(127, 2, '2020-03-07 04:24:02', 'El usuario admin eliminio el eje 7'),
(128, 2, '2020-03-07 04:24:05', 'El usuario admin eliminio el eje 8'),
(129, 2, '2020-03-07 04:24:05', 'El usuario admin eliminio el eje 8'),
(130, 2, '2020-03-07 04:24:05', 'El usuario admin eliminio el eje 8'),
(131, 2, '2020-03-07 04:24:05', 'El usuario admin eliminio el eje 8'),
(132, 2, '2020-03-07 04:24:05', 'El usuario admin eliminio el eje 8'),
(133, 2, '2020-03-07 04:24:05', 'El usuario admin eliminio el eje 8'),
(134, 2, '2020-03-07 04:24:05', 'El usuario admin eliminio el eje 8'),
(135, 2, '2020-03-07 04:24:08', 'El usuario admin eliminio el eje 9'),
(136, 2, '2020-03-07 04:24:08', 'El usuario admin eliminio el eje 9'),
(137, 2, '2020-03-07 04:24:08', 'El usuario admin eliminio el eje 9'),
(138, 2, '2020-03-07 04:24:08', 'El usuario admin eliminio el eje 9'),
(139, 2, '2020-03-07 04:24:08', 'El usuario admin eliminio el eje 9'),
(140, 2, '2020-03-07 04:24:08', 'El usuario admin eliminio el eje 9'),
(141, 2, '2020-03-07 04:24:13', 'El usuario admin eliminio el eje 10'),
(142, 2, '2020-03-07 04:24:13', 'El usuario admin eliminio el eje 10'),
(143, 2, '2020-03-07 04:24:13', 'El usuario admin eliminio el eje 10'),
(144, 2, '2020-03-07 04:24:13', 'El usuario admin eliminio el eje 10'),
(145, 2, '2020-03-07 04:24:13', 'El usuario admin eliminio el eje 10'),
(146, 2, '2020-03-07 04:24:16', 'El usuario admin eliminio el eje 11'),
(147, 2, '2020-03-07 04:24:16', 'El usuario admin eliminio el eje 11'),
(148, 2, '2020-03-07 04:24:16', 'El usuario admin eliminio el eje 11'),
(149, 2, '2020-03-07 04:24:16', 'El usuario admin eliminio el eje 11'),
(150, 2, '2020-03-07 04:24:20', 'El usuario admin eliminio el eje 12'),
(151, 2, '2020-03-07 04:24:20', 'El usuario admin eliminio el eje 12'),
(152, 2, '2020-03-07 04:24:20', 'El usuario admin eliminio el eje 12'),
(153, 2, '2020-03-07 04:24:23', 'El usuario admin eliminio el eje 13'),
(154, 2, '2020-03-07 04:24:23', 'El usuario admin eliminio el eje 13'),
(155, 2, '2020-03-07 04:46:13', 'El usuario admin creo el eje FP Formación Profesional'),
(156, 2, '2020-03-07 04:46:18', 'El usuario admin eliminio el eje 14'),
(157, 2, '2020-03-07 04:46:18', 'El usuario admin eliminio el eje 14'),
(158, 2, '2020-03-07 04:49:44', 'El usuario admin creo el eje FP Formación Profesional'),
(159, 2, '2020-03-07 04:49:56', 'El usuario admin creo el eje FP Formación Profesional'),
(160, 2, '2020-03-07 04:49:56', 'El usuario admin creo el eje FP Formación Profesional'),
(161, 2, '2020-03-07 15:25:29', 'El usuario admin creo la meta 1'),
(162, 2, '2020-03-07 15:25:37', 'El usuario admin creo la meta '),
(163, 2, '2020-03-07 15:26:01', 'El usuario admin creo la meta 1'),
(164, 2, '2020-03-07 15:35:13', 'El usuario admin creo la meta 1'),
(165, 2, '2020-03-07 15:35:35', 'El usuario admin creo la meta '),
(166, 2, '2020-03-07 15:38:19', 'El usuario admin creo la meta '),
(167, 2, '2020-03-07 15:39:42', 'El usuario admin creo la meta '),
(168, 2, '2020-03-07 15:43:06', 'El usuario admin creo la meta '),
(169, 2, '2020-03-07 15:43:10', 'El usuario admin eliminio la meta 1'),
(170, 2, '2020-03-07 15:43:19', 'El usuario admin creo la meta 1'),
(171, 2, '2020-03-07 15:44:27', 'El usuario admin creo la meta 2'),
(172, 2, '2020-03-07 15:47:03', 'El usuario admin creo la meta '),
(173, 2, '2020-03-07 15:47:03', 'El usuario admin creo la meta '),
(174, 2, '2020-03-07 15:47:17', 'El usuario admin creo la meta '),
(175, 2, '2020-03-07 15:47:18', 'El usuario admin creo la meta '),
(176, 2, '2020-03-07 17:06:23', 'El usuario admin creo la meta '),
(177, 2, '2020-03-07 17:06:23', 'El usuario admin creo la meta '),
(178, 2, '2020-03-07 17:06:33', 'El usuario admin creo la meta '),
(179, 2, '2020-03-07 17:06:33', 'El usuario admin creo la meta '),
(180, 2, '2020-03-07 17:08:08', 'El usuario admin creo la meta 2'),
(181, 2, '2020-03-07 17:08:08', 'El usuario admin creo la meta 2'),
(182, 2, '2020-03-07 18:46:01', 'El usuario admin creo la meta FP-I'),
(183, 2, '2020-03-07 18:46:01', 'El usuario admin creo la meta FP-I'),
(184, 2, '2020-03-07 18:46:11', 'El usuario admin creo la meta FP-I'),
(185, 2, '2020-03-07 18:46:11', 'El usuario admin creo la meta FP-I'),
(186, 2, '2020-03-07 18:47:44', 'El usuario admin creo la meta FP-I'),
(187, 2, '2020-03-07 18:49:14', 'El usuario admin creo la meta FP-II'),
(188, 2, '2020-03-08 16:52:27', 'El director admin creo el ususario   NomDepartamento CFG'),
(189, 2, '2020-03-08 17:32:32', 'El director admin creo el ususario   NomDepartamento FGGG'),
(190, 2, '2020-03-08 19:07:10', 'El usuario admin creo el departamento CFG'),
(191, 2, '2020-03-11 01:43:42', 'El usuario admin creo el objetivo zvd'),
(192, 2, '2020-03-11 02:13:56', 'El usuario admin eliminio el objetivo 1'),
(193, 2, '2020-03-11 02:15:04', 'El usuario admin creo el objetivo alfonso'),
(194, 2, '2020-03-11 02:18:12', 'El usuario admin creo el objetivo zdfsdGfsZ'),
(195, 2, '2020-03-11 02:24:44', 'El usuario admin creo el objetivo alfonso'),
(196, 2, '2020-03-11 02:24:44', 'El usuario admin creo el objetivo alfonso'),
(197, 2, '2020-03-11 02:24:54', 'El usuario admin creo el objetivo alfonso'),
(198, 2, '2020-03-11 02:24:55', 'El usuario admin creo el objetivo alfonso'),
(199, 2, '2020-03-11 02:25:01', 'El usuario admin eliminio el objetivo 3'),
(200, 2, '2020-03-11 02:25:01', 'El usuario admin eliminio el objetivo 3'),
(201, 2, '2020-03-11 02:25:13', 'El usuario admin creo el objetivo alfonso'),
(202, 2, '2020-03-11 02:25:21', 'El usuario admin creo el objetivo hasss'),
(203, 2, '2020-03-11 02:25:21', 'El usuario admin creo el objetivo hasss'),
(204, 2, '2020-03-11 02:30:59', 'El usuario admin creo el objetivo alfonso'),
(205, 2, '2020-03-11 02:30:59', 'El usuario admin creo el objetivo alfonso'),
(206, 2, '2020-03-11 03:53:12', 'El usuario admin creo la linea dgfff'),
(207, 2, '2020-03-11 03:53:19', 'El usuario admin creo la linea dgfff'),
(208, 2, '2020-03-11 03:53:23', 'El usuario admin eliminio la linea 0'),
(209, 2, '2020-03-11 03:53:38', 'El usuario admin creo la linea trabajo'),
(210, 2, '2020-03-12 20:19:07', 'El usuario admin creo objetivo sascsc'),
(211, 2, '2020-03-12 20:19:44', 'El usuario admin creo objetivo sascsc'),
(212, 2, '2020-03-12 20:19:47', 'El usuario admin eliminio objetivo 1'),
(213, 2, '2020-03-12 20:19:58', 'El usuario admin creo objetivo yaaaa'),
(214, 2, '2020-03-12 21:00:27', 'El usuario admin creo la actividad CFG 15/3/2020 fechafin 16/3/2020'),
(215, 2, '2020-03-12 21:03:19', 'El usuario admin eliminio el actividad 1'),
(216, 2, '2020-03-12 21:03:41', 'El usuario admin creo la actividad CFG 15/3/2020 fechafin 16/3/2020'),
(217, 2, '2020-03-12 21:48:50', 'El usuario admin creo objetivo yaaaa'),
(218, 2, '2020-03-13 00:22:29', 'El usuario admin creo la evidencia asfd'),
(219, 2, '2020-03-13 00:23:42', 'El usuario admin eliminio el eje 0'),
(220, 2, '2020-03-13 00:23:50', 'El usuario admin creo la evidencia asfbsb'),
(221, 2, '2020-03-13 00:23:56', 'El usuario admin creo la evidencia asfbsb'),
(222, 2, '2020-03-13 01:18:04', 'El usuario admin creo la tarea SALA'),
(223, 2, '2020-03-13 01:18:14', 'El usuario admin eliminio la tarea 1'),
(224, 2, '2020-03-13 01:18:14', 'El usuario admin eliminio la tarea 1'),
(225, 2, '2020-03-13 01:24:17', 'El usuario admin creo la tarea SALA2'),
(226, 2, '2020-03-13 02:01:33', 'El usuario admin creo el objetivo alfonso'),
(227, 2, '2020-03-13 02:01:55', 'El usuario admin creo el objetivo asdsa'),
(228, 2, '2020-03-13 02:09:33', 'El usuario admin creo el eje Investigación e Innovación'),
(229, 2, '2020-03-13 02:09:33', 'El usuario admin creo el eje Investigación e Innovación'),
(230, 2, '2020-03-13 02:09:49', 'El usuario admin creo el eje IN Investigación e Innovación'),
(231, 2, '2020-03-13 02:09:50', 'El usuario admin creo el eje IN Investigación e Innovación'),
(232, 2, '2020-03-13 02:10:28', 'El usuario admin creo el eje FP Formación Profesional'),
(233, 2, '2020-03-13 02:10:28', 'El usuario admin creo el eje FP Formación Profesional'),
(234, 2, '2020-03-13 02:10:47', 'El usuario admin creo el eje FP Formación Profesional'),
(235, 2, '2020-03-13 02:10:47', 'El usuario admin creo el eje FP Formación Profesional'),
(236, 2, '2020-03-13 02:11:13', 'El usuario admin creo el eje FP-I Formación Profesional'),
(237, 2, '2020-03-13 02:11:13', 'El usuario admin creo el eje FP-I Formación Profesional'),
(238, 2, '2020-03-13 02:11:25', 'El usuario admin creo el eje FP-II Formación Profesional'),
(239, 2, '2020-03-13 02:11:25', 'El usuario admin creo el eje FP-II Formación Profesional'),
(240, 2, '2020-03-13 02:11:43', 'El usuario admin creo el eje FP-II Formación Profesional'),
(241, 2, '2020-03-13 02:11:43', 'El usuario admin creo el eje FP-II Formación Profesional'),
(242, 2, '2020-03-13 02:12:36', 'El usuario admin creo el eje IN Investigación e Innovación'),
(243, 2, '2020-03-13 02:14:03', 'El usuario admin creo el eje FP-I '),
(244, 2, '2020-03-13 02:14:03', 'El usuario admin creo el eje FP-I '),
(245, 2, '2020-03-13 02:14:03', 'El usuario admin creo el eje FP-I '),
(246, 2, '2020-03-13 02:14:14', 'El usuario admin creo el eje FP-II Formación Profesional'),
(247, 2, '2020-03-13 02:14:14', 'El usuario admin creo el eje FP-II Formación Profesional'),
(248, 2, '2020-03-13 02:14:14', 'El usuario admin creo el eje FP-II Formación Profesional'),
(249, 2, '2020-03-13 02:14:30', 'El usuario admin creo el eje IN'),
(250, 2, '2020-03-13 02:14:30', 'El usuario admin creo el eje IN'),
(251, 2, '2020-03-13 02:14:30', 'El usuario admin creo el eje IN'),
(252, 2, '2020-03-13 02:14:44', 'El usuario admin creo el eje FP-II '),
(253, 2, '2020-03-13 02:14:44', 'El usuario admin creo el eje FP-II '),
(254, 2, '2020-03-13 02:14:44', 'El usuario admin creo el eje FP-II '),
(255, 2, '2020-03-13 02:17:05', 'El usuario admin creo el objetivo FP-I'),
(256, 2, '2020-03-13 02:17:05', 'El usuario admin creo el objetivo FP-I'),
(257, 2, '2020-03-13 02:17:05', 'El usuario admin creo el objetivo FP-I'),
(258, 2, '2020-03-13 02:18:30', 'El usuario admin creo el objetivo FP-II'),
(259, 2, '2020-03-13 02:18:30', 'El usuario admin creo el objetivo FP-II'),
(260, 2, '2020-03-13 02:18:30', 'El usuario admin creo el objetivo FP-II'),
(261, 2, '2020-03-13 02:19:23', 'El usuario admin eliminio el objetivo 4'),
(262, 2, '2020-03-13 02:19:23', 'El usuario admin eliminio el objetivo 4'),
(263, 2, '2020-03-13 02:19:23', 'El usuario admin eliminio el objetivo 4'),
(264, 2, '2020-03-13 02:19:28', 'El usuario admin eliminio el objetivo 5'),
(265, 2, '2020-03-13 02:19:28', 'El usuario admin eliminio el objetivo 5'),
(266, 2, '2020-03-13 02:20:23', 'El usuario admin creo el objetivo FP-II'),
(267, 2, '2020-03-13 02:20:59', 'El usuario admin creo el objetivo IN-I'),
(268, 2, '2020-03-13 02:47:42', 'El usuario admin creo objetivo FP-1.1.3 '),
(269, 2, '2020-03-13 02:48:06', 'El usuario admin creo objetivo FP- 1.2.2 '),
(270, 2, '2020-03-13 03:14:22', 'El usuario admin creo objetivo FP-2.1.4 '),
(271, 2, '2020-03-13 03:19:34', 'El usuario admin creo la linea FP- 1.1'),
(272, 2, '2020-03-13 03:19:59', 'El usuario admin creo la meta 1'),
(273, 2, '2020-03-13 03:19:59', 'El usuario admin creo la meta 1'),
(274, 2, '2020-03-13 03:19:59', 'El usuario admin creo la meta 1'),
(275, 2, '2020-03-13 03:19:59', 'El usuario admin creo la meta 1'),
(276, 2, '2020-03-13 03:20:16', 'El usuario admin creo la meta 2'),
(277, 2, '2020-03-13 03:20:16', 'El usuario admin creo la meta 2'),
(278, 2, '2020-03-13 03:20:16', 'El usuario admin creo la meta 2'),
(279, 2, '2020-03-13 03:20:16', 'El usuario admin creo la meta 2'),
(280, 2, '2020-03-13 03:20:23', 'El usuario admin creo la meta 2'),
(281, 2, '2020-03-13 03:20:23', 'El usuario admin creo la meta 2'),
(282, 2, '2020-03-13 03:20:23', 'El usuario admin creo la meta 2'),
(283, 2, '2020-03-13 03:20:23', 'El usuario admin creo la meta 2'),
(284, 2, '2020-03-13 03:20:27', 'El usuario admin eliminio la meta 4'),
(285, 2, '2020-03-13 03:20:28', 'El usuario admin eliminio la meta 4'),
(286, 2, '2020-03-13 03:20:28', 'El usuario admin eliminio la meta 4'),
(287, 2, '2020-03-13 03:20:28', 'El usuario admin eliminio la meta 4'),
(288, 2, '2020-03-13 03:20:32', 'El usuario admin eliminio la meta 5'),
(289, 2, '2020-03-13 03:20:32', 'El usuario admin eliminio la meta 5'),
(290, 2, '2020-03-13 03:20:32', 'El usuario admin eliminio la meta 5'),
(291, 2, '2020-03-13 03:31:25', 'El usuario admin creo el departamento Comisión Revisora'),
(292, 2, '2020-03-13 03:32:17', 'El usuario admin creo el departamento DEA'),
(293, 2, '2020-03-13 03:37:07', 'El usuario admin eliminio el director 1'),
(294, 2, '2020-03-13 03:37:07', 'El usuario admin eliminio el director 1'),
(295, 2, '2020-03-13 03:42:32', 'El director admin creo el ususario   NomDepartamento CFG'),
(296, 2, '2020-03-13 03:44:38', 'El director admin creo el ususario   NomDepartamento FGRE'),
(297, 2, '2020-03-13 15:31:49', 'El usuario admin creo la linea FP-1.2'),
(298, 2, '2020-03-13 15:32:18', 'El usuario admin creo la meta 3'),
(299, 2, '2020-03-13 15:59:30', 'El usuario admin creo la linea FP 2.1 '),
(300, 2, '2020-03-13 16:21:09', 'El usuario admin creo la linea 1.3 '),
(301, 2, '2020-03-13 16:21:30', 'El usuario admin creo la linea 1.4 '),
(302, 2, '2020-03-13 16:21:47', 'El usuario admin creo la linea 2.3 '),
(303, 2, '2020-03-13 17:02:51', 'El usuario admin creo la meta 1'),
(304, 2, '2020-03-13 17:02:59', 'El usuario admin creo la meta 1'),
(305, 2, '2020-03-13 17:04:41', 'El usuario admin creo el objetivo FP II'),
(306, 2, '2020-03-13 17:05:03', 'El usuario admin creo el eje FP'),
(307, 2, '2020-03-13 17:05:03', 'El usuario admin creo el eje FP'),
(308, 2, '2020-03-13 17:05:03', 'El usuario admin creo el eje FP'),
(309, 2, '2020-03-13 17:05:08', 'El usuario admin eliminio el eje 16'),
(310, 2, '2020-03-13 17:05:08', 'El usuario admin eliminio el eje 16'),
(311, 2, '2020-03-13 17:05:08', 'El usuario admin eliminio el eje 16'),
(312, 2, '2020-03-14 01:41:14', 'El usuario admin creo el objetivo zdfsdGfsZ'),
(313, 2, '2020-03-14 01:41:22', 'El usuario admin eliminio el objetivo 9'),
(314, 2, '2020-03-14 01:41:22', 'El usuario admin eliminio el objetivo 9'),
(315, 2, '2020-03-14 01:41:22', 'El usuario admin eliminio el objetivo 9'),
(316, 2, '2020-03-14 01:54:01', 'El usuario admin creo objetivo IN-1.3.4  '),
(317, 2, '2020-03-14 01:57:34', 'El usuario admin creo la linea dgfff'),
(318, 2, '2020-03-14 01:57:41', 'El usuario admin creo la linea dgfff'),
(319, 2, '2020-03-14 02:11:12', 'El usuario admin eliminio objetivo 9'),
(320, 2, '2020-03-14 02:11:12', 'El usuario admin eliminio objetivo 9'),
(321, 2, '2020-03-14 02:11:12', 'El usuario admin eliminio objetivo 9'),
(322, 2, '2020-03-14 02:11:19', 'El usuario admin eliminio la linea 8'),
(323, 2, '2020-03-14 02:11:19', 'El usuario admin eliminio la linea 8'),
(324, 2, '2020-03-14 02:11:19', 'El usuario admin eliminio la linea 8'),
(325, 2, '2020-03-14 02:11:24', 'El usuario admin eliminio la linea 9'),
(326, 2, '2020-03-14 02:11:24', 'El usuario admin eliminio la linea 9'),
(327, 2, '2020-03-14 02:19:20', 'El usuario admin creo el objetivo FP-II'),
(328, 2, '2020-03-14 02:19:20', 'El usuario admin creo el objetivo FP-II'),
(329, 2, '2020-03-14 02:40:49', 'El usuario admin creo la meta 1'),
(330, 2, '2020-03-14 02:41:14', 'El usuario admin creo la meta 1'),
(331, 2, '2020-03-14 02:41:31', 'El usuario admin creo la meta 1'),
(332, 2, '2020-03-14 03:23:43', 'El usuario admin creo la tarea SALA2'),
(333, 2, '2020-03-14 03:24:06', 'El usuario admin creo la tarea 1'),
(334, 2, '2020-03-14 03:24:27', 'El usuario admin creo la tarea 2'),
(335, 2, '2020-03-14 03:26:54', 'El usuario admin creo la tarea 1'),
(336, 2, '2020-03-14 03:26:54', 'El usuario admin creo la tarea 1'),
(337, 2, '2020-03-14 03:48:32', 'El usuario admin creo el actividad CFG'),
(338, 2, '2020-03-14 03:49:09', 'El usuario admin creo el actividad 1'),
(339, 2, '2020-03-14 03:51:18', 'El usuario admin creo la actividad 2 15/3/2020 fechafin 20/3/2020'),
(340, 2, '2020-03-14 03:54:05', 'El usuario admin creo el actividad 1'),
(341, 2, '2020-03-14 03:54:05', 'El usuario admin creo el actividad 1'),
(342, 2, '2020-03-14 03:54:13', 'El usuario admin creo el actividad 1'),
(343, 2, '2020-03-14 03:54:13', 'El usuario admin creo el actividad 1'),
(344, 2, '2020-03-14 03:54:28', 'El usuario admin creo el actividad 2'),
(345, 2, '2020-03-14 03:54:28', 'El usuario admin creo el actividad 2'),
(346, 2, '2020-03-14 03:55:25', 'El usuario admin creo la actividad 3 15/3/2020 fechafin 20/3/2020'),
(347, 2, '2020-03-14 03:55:34', 'El usuario admin eliminio el actividad 4'),
(348, 2, '2020-03-14 03:55:34', 'El usuario admin eliminio el actividad 4'),
(349, 2, '2020-03-14 03:55:34', 'El usuario admin eliminio el actividad 4'),
(350, 2, '2020-03-14 03:58:12', 'El usuario admin creo la tarea 1'),
(351, 2, '2020-03-14 03:58:12', 'El usuario admin creo la tarea 1'),
(352, 2, '2020-03-14 03:58:18', 'El usuario admin creo la tarea 1'),
(353, 2, '2020-03-14 03:58:18', 'El usuario admin creo la tarea 1'),
(354, 2, '2020-03-14 03:58:24', 'El usuario admin creo la tarea 2'),
(355, 2, '2020-03-14 03:58:24', 'El usuario admin creo la tarea 2'),
(356, 2, '2020-03-14 03:58:29', 'El usuario admin creo la tarea 1'),
(357, 2, '2020-03-14 03:58:29', 'El usuario admin creo la tarea 1'),
(358, 2, '2020-03-14 23:01:01', 'El usuario admin eliminio el actividad 3'),
(359, 2, '2020-03-14 23:01:01', 'El usuario admin eliminio el actividad 3'),
(360, 2, '2020-03-14 23:05:00', 'El usuario admin creo la actividad 2 2020-03-13 fechafin 2020-03-20'),
(361, 2, '2020-03-14 23:08:11', 'El usuario admin eliminio el actividad 5'),
(362, 2, '2020-03-14 23:08:11', 'El usuario admin eliminio el actividad 5'),
(363, 2, '2020-03-14 23:08:27', 'El usuario admin creo la actividad 2 2020-03-14 fechafin 2020-03-21'),
(364, 2, '2020-03-14 23:10:52', 'El usuario admin creo el eje FP'),
(365, 2, '2020-03-14 23:10:52', 'El usuario admin creo el eje FP'),
(366, 2, '2020-03-14 23:10:59', 'El usuario admin creo el eje FP'),
(367, 2, '2020-03-14 23:10:59', 'El usuario admin creo el eje FP'),
(368, 2, '2020-03-14 23:13:09', 'El usuario admin creo el actividad 2'),
(369, 2, '2020-03-14 23:13:09', 'El usuario admin creo el actividad 2'),
(370, 2, '2020-03-14 23:13:19', 'El usuario admin creo el actividad 2'),
(371, 2, '2020-03-14 23:13:19', 'El usuario admin creo el actividad 2'),
(372, 2, '2020-03-14 23:14:17', 'El usuario admin creo el actividad 2'),
(373, 2, '2020-03-14 23:14:17', 'El usuario admin creo el actividad 2'),
(374, 2, '2020-03-14 23:14:28', 'El usuario admin creo el actividad 2'),
(375, 2, '2020-03-14 23:14:28', 'El usuario admin creo el actividad 2'),
(376, 2, '2020-03-14 23:15:46', 'El usuario admin creo la actividad 3 2020-03-13 fechafin 2020-03-20'),
(377, 2, '2020-03-14 23:17:11', 'El usuario admin creo la tarea 1'),
(378, 2, '2020-03-14 23:17:11', 'El usuario admin creo la tarea 1'),
(379, 2, '2020-03-14 23:17:18', 'El usuario admin creo la tarea 1'),
(380, 2, '2020-03-14 23:17:18', 'El usuario admin creo la tarea 1'),
(381, 2, '2020-03-15 15:59:30', 'El usuario admin eliminio el actividad 7'),
(382, 2, '2020-03-15 15:59:30', 'El usuario admin eliminio el actividad 7'),
(383, 2, '2020-03-15 15:59:30', 'El usuario admin eliminio el actividad 7'),
(384, 2, '2020-03-15 15:59:46', 'El usuario admin creo la actividad 3 2020-03-21 fechafin 2020-03-20'),
(385, 2, '2020-03-15 15:59:52', 'El usuario admin creo el actividad 3'),
(386, 2, '2020-03-15 15:59:52', 'El usuario admin creo el actividad 3'),
(387, 2, '2020-03-15 15:59:52', 'El usuario admin creo el actividad 3'),
(388, 2, '2020-03-15 15:59:58', 'El usuario admin eliminio el actividad 8'),
(389, 2, '2020-03-15 15:59:58', 'El usuario admin eliminio el actividad 8'),
(390, 2, '2020-03-15 15:59:58', 'El usuario admin eliminio el actividad 8'),
(391, 2, '2020-03-15 16:00:13', 'El usuario admin creo la tarea 2'),
(392, 2, '2020-03-15 16:00:13', 'El usuario admin creo la tarea 2'),
(393, 2, '2020-03-15 16:00:19', 'El usuario admin creo la tarea 2'),
(394, 2, '2020-03-15 16:00:19', 'El usuario admin creo la tarea 2'),
(395, 2, '2020-03-15 16:02:53', 'El usuario admin creo el objetivo FP-II'),
(396, 2, '2020-03-15 16:02:53', 'El usuario admin creo el objetivo FP-II'),
(397, 2, '2020-03-15 16:03:00', 'El usuario admin creo el objetivo FP-II'),
(398, 2, '2020-03-15 16:03:00', 'El usuario admin creo el objetivo FP-II'),
(399, 2, '2020-03-15 16:03:09', 'El usuario admin creo el objetivo alfonso'),
(400, 2, '2020-03-15 16:03:14', 'El usuario admin eliminio el objetivo 10'),
(401, 2, '2020-03-15 16:03:14', 'El usuario admin eliminio el objetivo 10'),
(402, 2, '2020-03-15 16:03:14', 'El usuario admin eliminio el objetivo 10'),
(403, 2, '2020-03-15 16:03:27', 'El usuario admin creo objetivo FP-1.1.3 '),
(404, 2, '2020-03-15 16:03:27', 'El usuario admin creo objetivo FP-1.1.3 '),
(405, 2, '2020-03-15 16:03:36', 'El usuario admin creo objetivo FP-1.1.3 '),
(406, 2, '2020-03-15 16:03:36', 'El usuario admin creo objetivo FP-1.1.3 '),
(407, 2, '2020-03-15 16:18:12', 'El usuario admin creo la actividad 34 2020-03-21 fechafin 2020-03-29'),
(408, 2, '2020-03-15 16:18:18', 'El usuario admin eliminio el actividad 9'),
(409, 2, '2020-03-15 16:18:18', 'El usuario admin eliminio el actividad 9'),
(410, 2, '2020-03-15 16:18:18', 'El usuario admin eliminio el actividad 9'),
(411, 2, '2020-03-15 16:19:08', 'El usuario admin creo el actividad 2'),
(412, 2, '2020-03-15 16:19:08', 'El usuario admin creo el actividad 2'),
(413, 2, '2020-03-15 16:19:16', 'El usuario admin creo el actividad 2'),
(414, 2, '2020-03-15 16:19:16', 'El usuario admin creo el actividad 2'),
(415, 2, '2020-03-16 14:13:17', 'El usuario admin creo el objetivo afkjhwe'),
(416, 2, '2020-03-16 14:13:32', 'El usuario admin eliminio el objetivo 11'),
(417, 2, '2020-03-16 14:13:32', 'El usuario admin eliminio el objetivo 11'),
(418, 2, '2020-03-16 14:13:32', 'El usuario admin eliminio el objetivo 11'),
(419, 2, '2020-03-17 19:36:03', 'El usuario admin creo el director yohany'),
(420, 2, '2020-03-17 19:36:03', 'El usuario admin creo el director yohany'),
(421, 2, '2020-03-17 19:36:03', 'El usuario admin creo el director yohany'),
(422, 2, '2020-03-17 19:39:04', 'El usuario admin eliminio el director 4'),
(423, 2, '2020-03-17 19:39:04', 'El usuario admin eliminio el director 4'),
(424, 2, '2020-03-17 19:39:04', 'El usuario admin eliminio el director 4'),
(425, 2, '2020-03-17 19:39:23', 'El director admin creo el ususario   NomDepartamento CFDDD'),
(426, 2, '2020-03-17 19:39:43', 'El usuario admin creo el director yohany'),
(427, 2, '2020-03-17 19:39:44', 'El usuario admin creo el director yohany'),
(428, 2, '2020-03-17 19:39:44', 'El usuario admin creo el director yohany'),
(429, 2, '2020-03-17 19:39:57', 'El usuario admin creo el director yohany'),
(430, 2, '2020-03-17 19:39:57', 'El usuario admin creo el director yohany'),
(431, 2, '2020-03-17 19:39:57', 'El usuario admin creo el director yohany'),
(432, 2, '2020-03-17 19:40:02', 'El usuario admin eliminio el director 2'),
(433, 2, '2020-03-17 19:40:02', 'El usuario admin eliminio el director 2'),
(434, 2, '2020-03-17 19:40:02', 'El usuario admin eliminio el director 2'),
(435, 2, '2020-03-17 19:40:17', 'El director admin creo el ususario   NomDepartamento DDDD'),
(436, 2, '2020-03-17 19:42:14', 'El usuario admin eliminio el director 6'),
(437, 2, '2020-03-17 19:42:14', 'El usuario admin eliminio el director 6'),
(438, 2, '2020-03-17 19:42:14', 'El usuario admin eliminio el director 6'),
(439, 2, '2020-03-17 19:42:27', 'El director admin creo el ususario   NomDepartamento DDDFF'),
(440, 2, '2020-03-17 19:43:18', 'El usuario admin creo el director yohany'),
(441, 2, '2020-03-17 19:43:18', 'El usuario admin creo el director yohany'),
(442, 2, '2020-03-17 19:43:18', 'El usuario admin creo el director yohany'),
(443, 2, '2020-03-17 19:53:10', 'El usuario admin creo el departamento CFGGgggacA'),
(444, 2, '2020-03-17 20:03:49', 'El usuario admin creo el actividad 1'),
(445, 2, '2020-03-17 20:03:49', 'El usuario admin creo el actividad 1'),
(446, 2, '2020-03-17 20:04:00', 'El usuario admin creo la tarea 1'),
(447, 2, '2020-03-17 20:04:00', 'El usuario admin creo la tarea 1'),
(448, 2, '2020-03-17 20:04:00', 'El usuario admin creo la tarea 1'),
(449, 2, '2020-03-17 20:04:20', 'El usuario admin creo el director Pedro Emilio1'),
(450, 2, '2020-03-17 20:04:20', 'El usuario admin creo el director Pedro Emilio1'),
(451, 2, '2020-03-17 20:04:20', 'El usuario admin creo el director Pedro Emilio1'),
(452, 2, '2020-03-17 20:04:27', 'El usuario admin creo el director Pedro Emilio'),
(453, 2, '2020-03-17 20:04:27', 'El usuario admin creo el director Pedro Emilio'),
(454, 2, '2020-03-17 20:04:27', 'El usuario admin creo el director Pedro Emilio'),
(455, 2, '2020-03-18 00:15:23', 'El usuario admin creo el actividad 1'),
(456, 2, '2020-03-18 00:15:23', 'El usuario admin creo el actividad 1'),
(457, 2, '2020-03-18 00:15:35', 'El usuario admin creo la tarea 1'),
(458, 2, '2020-03-18 00:15:35', 'El usuario admin creo la tarea 1'),
(459, 2, '2020-03-18 00:15:41', 'El usuario admin creo la tarea 1'),
(460, 2, '2020-03-18 00:15:41', 'El usuario admin creo la tarea 1'),
(461, 2, '2020-03-18 00:16:16', 'El usuario admin creo la actividad 35 2020-03-18 fechafin 2020-03-19'),
(462, 2, '2020-03-18 00:16:25', 'El usuario admin creo el actividad 35'),
(463, 2, '2020-03-18 00:16:26', 'El usuario admin creo el actividad 35'),
(464, 2, '2020-03-18 00:16:26', 'El usuario admin creo el actividad 35'),
(465, 2, '2020-03-18 00:16:30', 'El usuario admin eliminio el actividad 10'),
(466, 2, '2020-03-18 00:16:30', 'El usuario admin eliminio el actividad 10'),
(467, 2, '2020-03-18 00:16:30', 'El usuario admin eliminio el actividad 10'),
(468, 2, '2020-03-18 00:16:43', 'El usuario admin creo la tarea vnhny'),
(469, 2, '2020-03-18 00:16:49', 'El usuario admin creo la tarea vnhny'),
(470, 2, '2020-03-18 00:16:49', 'El usuario admin creo la tarea vnhny'),
(471, 2, '2020-03-18 00:16:49', 'El usuario admin creo la tarea vnhny'),
(472, 2, '2020-03-18 00:16:53', 'El usuario admin eliminio la tarea 4'),
(473, 2, '2020-03-18 00:16:53', 'El usuario admin eliminio la tarea 4'),
(474, 2, '2020-03-18 00:16:54', 'El usuario admin eliminio la tarea 4'),
(475, 2, '2020-03-18 00:21:52', 'El usuario admin creo la tarea 2'),
(476, 2, '2020-03-18 00:21:52', 'El usuario admin creo la tarea 2'),
(477, 2, '2020-03-18 00:22:11', 'El usuario admin creo la tarea 1'),
(478, 2, '2020-03-18 00:22:11', 'El usuario admin creo la tarea 1'),
(479, 2, '2020-03-18 01:12:44', 'El usuario admin creo el actividad 1'),
(480, 2, '2020-03-18 01:12:51', 'El usuario admin creo el actividad 1'),
(481, 2, '2020-03-18 02:35:37', 'El usuario admin creo el director Jose'),
(482, 2, '2020-03-18 02:35:37', 'El usuario admin creo el director Jose'),
(483, 2, '2020-03-18 02:35:37', 'El usuario admin creo el director Jose'),
(484, 2, '2020-03-18 02:39:57', 'El usuario admin creo el departamento CFGddddd'),
(485, 2, '2020-03-18 18:47:41', 'El usuario admin creo el director Pedro EmilioW'),
(486, 2, '2020-03-18 18:47:41', 'El usuario admin creo el director Pedro EmilioW'),
(487, 2, '2020-03-18 18:47:41', 'El usuario admin creo el director Pedro EmilioW'),
(488, 2, '2020-03-18 18:47:47', 'El usuario admin creo el director Pedro Emilio'),
(489, 2, '2020-03-18 18:47:47', 'El usuario admin creo el director Pedro Emilio'),
(490, 2, '2020-03-18 18:47:47', 'El usuario admin creo el director Pedro Emilio'),
(491, 2, '2020-03-18 18:47:59', 'El director admin creo el ususario   NomDepartamento AS'),
(492, 2, '2020-03-18 18:48:12', 'El usuario admin eliminio el director 8'),
(493, 2, '2020-03-18 18:48:12', 'El usuario admin eliminio el director 8'),
(494, 2, '2020-03-18 18:48:12', 'El usuario admin eliminio el director 8'),
(495, 2, '2020-03-18 18:48:12', 'El usuario admin eliminio el director 8'),
(496, 2, '2020-03-18 18:53:41', 'El usuario admin creo el departamento SSSS'),
(497, 2, '2020-03-18 19:16:30', 'El usuario admin creo el departamento Vicerrectoría Académica'),
(498, 2, '2020-03-18 19:16:49', 'El usuario admin creo el departamento CUDE'),
(499, 2, '2020-03-18 19:17:09', 'El usuario admin creo el departamento Direcciones de Escuelas'),
(500, 2, '2020-03-18 19:17:36', 'El usuario admin creo el departamento Gestión Humana'),
(501, 2, '2020-03-18 19:18:06', 'El usuario admin creo el departamento Aseguramiento de la Calidad'),
(502, 2, '2020-03-18 19:18:39', 'El usuario admin creo el departamento CINGEP'),
(503, 2, '2020-03-18 19:19:18', 'El usuario admin creo el departamento Investigación Científica'),
(504, 2, '2020-03-18 19:26:17', 'El usuario admin creo el director Jose'),
(505, 2, '2020-03-18 19:26:17', 'El usuario admin creo el director Jose'),
(506, 2, '2020-03-18 19:26:17', 'El usuario admin creo el director Jose'),
(507, 2, '2020-03-18 19:26:25', 'El usuario admin creo el director Jose'),
(508, 2, '2020-03-18 19:26:25', 'El usuario admin creo el director Jose'),
(509, 2, '2020-03-18 19:26:25', 'El usuario admin creo el director Jose'),
(510, 2, '2020-03-18 19:41:12', 'El usuario admin creo el director Pedro Emilio'),
(511, 2, '2020-03-18 19:41:12', 'El usuario admin creo el director Pedro Emilio'),
(512, 2, '2020-03-18 19:41:12', 'El usuario admin creo el director Pedro Emilio'),
(513, 2, '2020-03-18 19:41:26', 'El usuario admin creo el director Pedro Emilio'),
(514, 2, '2020-03-18 19:41:26', 'El usuario admin creo el director Pedro Emilio'),
(515, 2, '2020-03-18 19:41:26', 'El usuario admin creo el director Pedro Emilio'),
(516, 2, '2020-03-18 19:41:40', 'El usuario admin eliminio el director 5'),
(517, 2, '2020-03-18 19:41:41', 'El usuario admin eliminio el director 5'),
(518, 2, '2020-03-18 19:41:41', 'El usuario admin eliminio el director 5'),
(519, 2, '2020-03-18 19:41:50', 'El usuario admin eliminio el director 3'),
(520, 2, '2020-03-18 19:41:50', 'El usuario admin eliminio el director 3'),
(521, 2, '2020-03-18 19:46:06', 'El usuario admin creo el director yohany'),
(522, 2, '2020-03-18 19:46:22', 'El director admin creo el ususario   NomDepartamento CFG'),
(523, 2, '2020-03-18 19:46:42', 'El usuario admin creo el director Pedro Emilio'),
(524, 2, '2020-03-18 19:46:42', 'El usuario admin creo el director Pedro Emilio'),
(525, 2, '2020-03-18 19:47:30', 'El usuario admin creo el director Pedro Emilio'),
(526, 2, '2020-03-18 19:47:30', 'El usuario admin creo el director Pedro Emilio'),
(527, 2, '2020-03-18 19:47:37', 'El usuario admin eliminio el director 9'),
(528, 2, '2020-03-18 19:47:37', 'El usuario admin eliminio el director 9'),
(529, 2, '2020-03-18 19:47:47', 'El director admin creo el ususario   NomDepartamento CFG'),
(530, 2, '2020-03-18 19:48:15', 'El usuario admin creo el director yovanny'),
(531, 2, '2020-03-18 19:48:15', 'El usuario admin creo el director yovanny'),
(532, 2, '2020-03-18 19:50:39', 'El usuario admin creo el director Jovanny Rodríguez'),
(533, 2, '2020-03-18 19:50:39', 'El usuario admin creo el director Jovanny Rodríguez'),
(534, 2, '2020-03-18 19:53:16', 'El director admin creo el ususario   NomDepartamento CINGEP'),
(535, 2, '2020-03-18 20:06:06', 'El usuario admin creo el director Pedro Emilio'),
(536, 2, '2020-03-18 20:06:06', 'El usuario admin creo el director Pedro Emilio'),
(537, 2, '2020-03-18 20:06:07', 'El usuario admin creo el director Pedro Emilio'),
(538, 2, '2020-03-18 20:06:14', 'El usuario admin creo el director Pedro Emilio'),
(539, 2, '2020-03-18 20:06:14', 'El usuario admin creo el director Pedro Emilio'),
(540, 2, '2020-03-18 20:06:14', 'El usuario admin creo el director Pedro Emilio'),
(541, 2, '2020-03-18 20:06:18', 'El usuario admin eliminio el director 11'),
(542, 2, '2020-03-18 20:06:19', 'El usuario admin eliminio el director 11'),
(543, 2, '2020-03-18 20:06:19', 'El usuario admin eliminio el director 11'),
(544, 2, '2020-03-18 20:08:12', 'El director admin creo el ususario   NomDepartamento CINGEP'),
(545, 2, '2020-03-18 20:25:55', 'El usuario admin creo el eje FP'),
(546, 2, '2020-03-18 20:25:55', 'El usuario admin creo el eje FP'),
(547, 2, '2020-03-18 20:26:01', 'El usuario admin creo el eje FP'),
(548, 2, '2020-03-18 20:26:01', 'El usuario admin creo el eje FP'),
(549, 2, '2020-03-18 20:26:38', 'El usuario admin creo objetivo FP-1.1.3 '),
(550, 2, '2020-03-18 20:26:38', 'El usuario admin creo objetivo FP-1.1.3 '),
(551, 2, '2020-03-18 20:26:43', 'El usuario admin creo objetivo FP-1.1.3 '),
(552, 2, '2020-03-18 20:26:43', 'El usuario admin creo objetivo FP-1.1.3 '),
(553, 2, '2020-03-18 20:29:43', 'El usuario admin creo objetivo ss'),
(554, 2, '2020-03-18 20:29:47', 'El usuario admin eliminio objetivo 10'),
(555, 2, '2020-03-18 20:29:47', 'El usuario admin eliminio objetivo 10'),
(556, 2, '2020-03-18 20:29:47', 'El usuario admin eliminio objetivo 10'),
(557, 2, '2020-03-19 12:41:42', 'El usuario admin creo el actividad 1'),
(558, 2, '2020-03-19 12:41:52', 'El usuario admin creo el actividad 1'),
(559, 2, '2020-03-19 12:51:07', 'El usuario admin creo el actividad 1'),
(560, 2, '2020-03-19 12:51:14', 'El usuario admin creo el actividad 1'),
(561, 2, '2020-03-19 12:53:53', 'El usuario admin creo la tarea 1'),
(562, 2, '2020-03-19 12:53:54', 'El usuario admin creo la tarea 1'),
(563, 2, '2020-03-19 12:54:01', 'El usuario admin creo la tarea 1'),
(564, 2, '2020-03-19 12:54:01', 'El usuario admin creo la tarea 1'),
(565, 2, '2020-03-19 12:59:24', 'El usuario admin creo la meta 1'),
(566, 2, '2020-03-19 12:59:32', 'El usuario admin creo la meta 1'),
(567, 2, '2020-03-19 13:00:08', 'El usuario admin creo el eje 22'),
(568, 2, '2020-03-19 13:00:14', 'El usuario admin eliminio el eje 18'),
(569, 2, '2020-03-19 13:00:14', 'El usuario admin eliminio el eje 18'),
(570, 2, '2020-03-19 13:00:14', 'El usuario admin eliminio el eje 18'),
(571, 2, '2020-03-19 13:03:26', 'El usuario admin creo el objetivo 2'),
(572, 2, '2020-03-19 13:03:31', 'El usuario admin eliminio el objetivo 12'),
(573, 2, '2020-03-19 13:03:31', 'El usuario admin eliminio el objetivo 12'),
(574, 2, '2020-03-19 13:03:31', 'El usuario admin eliminio el objetivo 12'),
(575, 2, '2020-03-19 13:03:48', 'El usuario admin creo objetivo 2aaca'),
(576, 2, '2020-03-19 13:03:53', 'El usuario admin eliminio objetivo 11'),
(577, 2, '2020-03-19 13:03:53', 'El usuario admin eliminio objetivo 11'),
(578, 2, '2020-03-19 13:03:53', 'El usuario admin eliminio objetivo 11'),
(579, 2, '2020-03-19 13:04:03', 'El usuario admin creo la linea ss'),
(580, 2, '2020-03-19 13:04:08', 'El usuario admin eliminio la linea 10'),
(581, 2, '2020-03-19 13:04:09', 'El usuario admin eliminio la linea 10'),
(582, 2, '2020-03-19 23:13:44', 'El usuario admin creo el eje ssss'),
(583, 2, '2020-03-19 23:14:00', 'El usuario admin eliminio el eje 19'),
(584, 2, '2020-03-19 23:14:00', 'El usuario admin eliminio el eje 19'),
(585, 2, '2020-03-19 23:14:00', 'El usuario admin eliminio el eje 19'),
(586, 2, '2020-03-19 23:31:41', 'El usuario admin eliminio el departamento 1'),
(587, 2, '2020-03-19 23:31:41', 'El usuario admin eliminio el departamento 1'),
(588, 2, '2020-03-19 23:31:41', 'El usuario admin eliminio el departamento 1'),
(589, 2, '2020-03-19 23:31:41', 'El usuario admin eliminio el departamento 1'),
(590, 2, '2020-03-19 23:31:41', 'El usuario admin eliminio el departamento 1'),
(591, 2, '2020-03-19 23:31:41', 'El usuario admin eliminio el departamento 1'),
(592, 2, '2020-03-19 23:31:41', 'El usuario admin eliminio el departamento 1'),
(593, 2, '2020-03-19 23:31:41', 'El usuario admin eliminio el departamento 1'),
(594, 2, '2020-03-19 23:31:41', 'El usuario admin eliminio el departamento 1'),
(595, 2, '2020-03-19 23:31:41', 'El usuario admin eliminio el departamento 1'),
(596, 2, '2020-03-19 23:31:41', 'El usuario admin eliminio el departamento 1'),
(597, 2, '2020-03-19 23:31:51', 'El usuario admin eliminio el departamento 1'),
(598, 2, '2020-03-19 23:31:51', 'El usuario admin eliminio el departamento 1'),
(599, 2, '2020-03-19 23:31:51', 'El usuario admin eliminio el departamento 1'),
(600, 2, '2020-03-19 23:31:51', 'El usuario admin eliminio el departamento 1'),
(601, 2, '2020-03-19 23:31:51', 'El usuario admin eliminio el departamento 1'),
(602, 2, '2020-03-19 23:31:51', 'El usuario admin eliminio el departamento 1'),
(603, 2, '2020-03-19 23:31:51', 'El usuario admin eliminio el departamento 1'),
(604, 2, '2020-03-19 23:31:51', 'El usuario admin eliminio el departamento 1'),
(605, 2, '2020-03-19 23:31:51', 'El usuario admin eliminio el departamento 1'),
(606, 2, '2020-03-19 23:31:51', 'El usuario admin eliminio el departamento 1'),
(607, 2, '2020-03-19 23:32:01', 'El usuario admin eliminio el departamento 5'),
(608, 2, '2020-03-19 23:32:01', 'El usuario admin eliminio el departamento 5'),
(609, 2, '2020-03-19 23:32:01', 'El usuario admin eliminio el departamento 5'),
(610, 2, '2020-03-19 23:32:01', 'El usuario admin eliminio el departamento 5'),
(611, 2, '2020-03-19 23:32:01', 'El usuario admin eliminio el departamento 5'),
(612, 2, '2020-03-19 23:32:01', 'El usuario admin eliminio el departamento 5'),
(613, 2, '2020-03-19 23:32:01', 'El usuario admin eliminio el departamento 5'),
(614, 2, '2020-03-19 23:32:02', 'El usuario admin eliminio el departamento 5'),
(615, 2, '2020-03-19 23:32:02', 'El usuario admin eliminio el departamento 5'),
(616, 2, '2020-03-19 23:32:02', 'El usuario admin eliminio el departamento 5'),
(617, 2, '2020-03-19 23:32:24', 'El usuario admin creo el departamento CFG'),
(618, 2, '2020-03-19 23:32:35', 'El usuario admin eliminio el director 10'),
(619, 2, '2020-03-19 23:32:35', 'El usuario admin eliminio el director 10'),
(620, 2, '2020-03-19 23:32:35', 'El usuario admin eliminio el director 10'),
(621, 2, '2020-03-19 23:32:45', 'El director admin creo el ususario   NomDepartamento CINGEP'),
(622, 2, '2020-03-19 23:33:47', 'El director admin creo el ususario   NomDepartamento CINGEP'),
(623, 2, '2020-03-19 23:33:58', 'El usuario admin eliminio el director 13'),
(624, 2, '2020-03-19 23:33:58', 'El usuario admin eliminio el director 13'),
(625, 2, '2020-03-19 23:33:58', 'El usuario admin eliminio el director 13'),
(626, 2, '2020-03-19 23:33:59', 'El usuario admin eliminio el director 13'),
(627, 2, '2020-03-19 23:34:03', 'El usuario admin eliminio el director 14'),
(628, 2, '2020-03-19 23:34:03', 'El usuario admin eliminio el director 14'),
(629, 2, '2020-03-19 23:34:04', 'El usuario admin eliminio el director 14'),
(630, 2, '2020-03-19 23:34:12', 'El director admin creo el ususario   NomDepartamento CINGEP'),
(631, 2, '2020-03-19 23:38:04', 'El usuario admin creo la meta 4'),
(632, 2, '2020-03-19 23:38:15', 'El usuario admin eliminio la meta 13'),
(633, 2, '2020-03-19 23:38:15', 'El usuario admin eliminio la meta 13'),
(634, 2, '2020-03-19 23:38:15', 'El usuario admin eliminio la meta 13'),
(635, 2, '2020-03-19 23:38:15', 'El usuario admin eliminio la meta 13'),
(636, 2, '2020-03-19 23:38:28', 'El usuario admin creo el eje e'),
(637, 2, '2020-03-19 23:38:33', 'El usuario admin eliminio el eje 20'),
(638, 2, '2020-03-19 23:38:33', 'El usuario admin eliminio el eje 20'),
(639, 2, '2020-03-19 23:38:33', 'El usuario admin eliminio el eje 20'),
(640, 2, '2020-03-19 23:38:44', 'El usuario admin creo el objetivo rr'),
(641, 2, '2020-03-19 23:38:51', 'El usuario admin eliminio el objetivo 13'),
(642, 2, '2020-03-19 23:38:51', 'El usuario admin eliminio el objetivo 13'),
(643, 2, '2020-03-19 23:38:51', 'El usuario admin eliminio el objetivo 13'),
(644, 2, '2020-03-19 23:39:01', 'El usuario admin creo objetivo hhgg'),
(645, 2, '2020-03-19 23:39:07', 'El usuario admin eliminio objetivo 12'),
(646, 2, '2020-03-19 23:39:07', 'El usuario admin eliminio objetivo 12'),
(647, 2, '2020-03-19 23:39:08', 'El usuario admin eliminio objetivo 12'),
(648, 2, '2020-03-19 23:39:18', 'El usuario admin creo la linea dd'),
(649, 2, '2020-03-19 23:39:24', 'El usuario admin eliminio la linea 11'),
(650, 2, '2020-03-19 23:39:24', 'El usuario admin eliminio la linea 11'),
(651, 2, '2020-03-19 23:42:45', 'El usuario admin eliminio el actividad 12'),
(652, 2, '2020-03-19 23:42:45', 'El usuario admin eliminio el actividad 12'),
(653, 2, '2020-03-19 23:42:59', 'El usuario admin creo la actividad 2 2020-03-20 fechafin 2020-03-27'),
(654, 2, '2020-03-19 23:43:08', 'El usuario admin eliminio el actividad 13'),
(655, 2, '2020-03-19 23:43:08', 'El usuario admin eliminio el actividad 13'),
(656, 2, '2020-03-20 01:09:50', 'El usuario admin creo la actividad 2 2020-03-20 fechafin 2020-03-27'),
(657, 2, '2020-03-20 03:18:09', 'El usuario admin creo el director Pedro Emilio'),
(658, 2, '2020-03-20 03:18:09', 'El usuario admin creo el director Pedro Emilio'),
(659, 2, '2020-03-20 03:18:09', 'El usuario admin creo el director Pedro Emilio'),
(660, 2, '2020-03-21 00:22:11', 'El usuario admin creo el eje as;'),
(661, 2, '2020-03-21 00:22:29', 'El usuario admin eliminio el eje 21'),
(662, 2, '2020-03-21 00:22:29', 'El usuario admin eliminio el eje 21'),
(663, 2, '2020-03-21 00:22:29', 'El usuario admin eliminio el eje 21'),
(664, 2, '2020-03-21 00:26:02', 'El usuario admin creo el eje FP'),
(665, 2, '2020-03-21 00:26:02', 'El usuario admin creo el eje FP'),
(666, 2, '2020-03-21 00:26:09', 'El usuario admin creo el eje FP'),
(667, 2, '2020-03-21 00:26:09', 'El usuario admin creo el eje FP'),
(668, 2, '2020-03-21 00:30:06', 'El usuario admin creo la actividad 3 2020-03-01 fechafin 2020-04-09'),
(669, 2, '2020-03-21 00:31:09', 'El usuario admin eliminio el actividad 15'),
(670, 2, '2020-03-21 00:31:09', 'El usuario admin eliminio el actividad 15'),
(671, 2, '2020-03-21 00:31:09', 'El usuario admin eliminio el actividad 15'),
(672, 2, '2020-03-21 00:33:59', 'El usuario admin creo el departamento sssssssss'),
(673, 2, '2020-03-21 00:49:35', 'El usuario admin creo el actividad 1'),
(674, 2, '2020-03-21 00:49:35', 'El usuario admin creo el actividad 1'),
(675, 2, '2020-03-21 03:35:23', 'El usuario admin creo la tarea '),
(676, 2, '2020-03-21 03:35:24', 'El usuario admin creo la tarea '),
(677, 2, '2020-03-21 03:37:54', 'El usuario admin creo la tarea '),
(678, 2, '2020-03-21 03:37:54', 'El usuario admin creo la tarea '),
(679, 2, '2020-03-21 03:39:35', 'El usuario admin creo la tarea '),
(680, 2, '2020-03-21 03:57:31', 'El usuario admin eliminio la tarea 9'),
(681, 2, '2020-03-21 03:57:31', 'El usuario admin eliminio la tarea 9');
INSERT INTO `bitacora` (`id_bitacora`, `id_usuario`, `fecha`, `accion`) VALUES
(682, 2, '2020-03-21 03:57:31', 'El usuario admin eliminio la tarea 9'),
(683, 2, '2020-03-21 03:57:31', 'El usuario admin eliminio la tarea 9'),
(684, 2, '2020-03-21 03:57:31', 'El usuario admin eliminio la tarea 9'),
(685, 2, '2020-03-21 03:57:31', 'El usuario admin eliminio la tarea 9'),
(686, 2, '2020-03-21 03:57:32', 'El usuario admin eliminio la tarea 9'),
(687, 2, '2020-03-21 03:57:32', 'El usuario admin eliminio la tarea 9'),
(688, 2, '2020-03-21 03:57:32', 'El usuario admin eliminio la tarea 9'),
(689, 2, '2020-03-21 03:57:32', 'El usuario admin eliminio la tarea 9'),
(690, 2, '2020-03-21 03:57:32', 'El usuario admin eliminio la tarea 9'),
(691, 2, '2020-03-21 03:57:32', 'El usuario admin eliminio la tarea 9'),
(692, 2, '2020-03-21 03:57:32', 'El usuario admin eliminio la tarea 9'),
(693, 2, '2020-03-21 03:57:32', 'El usuario admin eliminio la tarea 9'),
(694, 2, '2020-03-21 03:57:40', 'El usuario admin eliminio la tarea 8'),
(695, 2, '2020-03-21 03:57:40', 'El usuario admin eliminio la tarea 8'),
(696, 2, '2020-03-21 03:57:40', 'El usuario admin eliminio la tarea 8'),
(697, 2, '2020-03-21 03:57:40', 'El usuario admin eliminio la tarea 8'),
(698, 2, '2020-03-21 03:57:40', 'El usuario admin eliminio la tarea 8'),
(699, 2, '2020-03-21 03:57:40', 'El usuario admin eliminio la tarea 8'),
(700, 2, '2020-03-21 03:57:40', 'El usuario admin eliminio la tarea 8'),
(701, 2, '2020-03-21 03:57:40', 'El usuario admin eliminio la tarea 8'),
(702, 2, '2020-03-21 03:57:40', 'El usuario admin eliminio la tarea 8'),
(703, 2, '2020-03-21 03:57:40', 'El usuario admin eliminio la tarea 8'),
(704, 2, '2020-03-21 03:57:40', 'El usuario admin eliminio la tarea 8'),
(705, 2, '2020-03-21 03:57:40', 'El usuario admin eliminio la tarea 8'),
(706, 2, '2020-03-21 03:57:47', 'El usuario admin eliminio la tarea 7'),
(707, 2, '2020-03-21 03:57:47', 'El usuario admin eliminio la tarea 7'),
(708, 2, '2020-03-21 03:57:47', 'El usuario admin eliminio la tarea 7'),
(709, 2, '2020-03-21 03:57:47', 'El usuario admin eliminio la tarea 7'),
(710, 2, '2020-03-21 03:57:47', 'El usuario admin eliminio la tarea 7'),
(711, 2, '2020-03-21 03:57:47', 'El usuario admin eliminio la tarea 7'),
(712, 2, '2020-03-21 03:57:47', 'El usuario admin eliminio la tarea 7'),
(713, 2, '2020-03-21 03:57:47', 'El usuario admin eliminio la tarea 7'),
(714, 2, '2020-03-21 03:57:47', 'El usuario admin eliminio la tarea 7'),
(715, 2, '2020-03-21 03:57:47', 'El usuario admin eliminio la tarea 7'),
(716, 2, '2020-03-21 03:57:55', 'El usuario admin eliminio la tarea 6'),
(717, 2, '2020-03-21 03:57:55', 'El usuario admin eliminio la tarea 6'),
(718, 2, '2020-03-21 03:57:55', 'El usuario admin eliminio la tarea 6'),
(719, 2, '2020-03-21 03:57:55', 'El usuario admin eliminio la tarea 6'),
(720, 2, '2020-03-21 03:57:55', 'El usuario admin eliminio la tarea 6'),
(721, 2, '2020-03-21 03:57:55', 'El usuario admin eliminio la tarea 6'),
(722, 2, '2020-03-21 03:57:55', 'El usuario admin eliminio la tarea 6'),
(723, 2, '2020-03-21 03:57:55', 'El usuario admin eliminio la tarea 6'),
(724, 2, '2020-03-21 03:58:02', 'El usuario admin eliminio la tarea 5'),
(725, 2, '2020-03-21 03:58:02', 'El usuario admin eliminio la tarea 5'),
(726, 2, '2020-03-21 03:58:02', 'El usuario admin eliminio la tarea 5'),
(727, 2, '2020-03-21 03:58:02', 'El usuario admin eliminio la tarea 5'),
(728, 2, '2020-03-21 03:58:02', 'El usuario admin eliminio la tarea 5'),
(729, 2, '2020-03-21 03:58:02', 'El usuario admin eliminio la tarea 5');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `IdDeparta` int(11) NOT NULL,
  `NomDeparta` varchar(50) NOT NULL,
  `DescDeparta` varchar(200) NOT NULL,
  `Estado` enum('Activo','Inactivo','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`IdDeparta`, `NomDeparta`, `DescDeparta`, `Estado`) VALUES
(2, 'Comisión Revisora', 'Revisión de proyectos', 'Activo'),
(3, 'DEA', 'Protocolos de elaboracion', 'Inactivo'),
(7, 'Vicerrectoría Académica', 'Procedimientos', 'Activo'),
(8, 'CUDE', 'Resultados', 'Activo'),
(9, 'Direcciones de Escuelas', 'Estrategias', 'Activo'),
(10, 'Gestión Humana', 'Personal', 'Activo'),
(11, 'Aseguramiento de la Calidad', 'Personal', 'Activo'),
(12, 'CINGEP', 'Diseño del curso por Escuela', 'Activo'),
(13, 'Investigación Científica', 'Orientar la elaboración de artículos científicos', 'Activo'),
(14, 'CFG', 'hfdss', 'Activo'),
(15, 'sssssssss', 'ssssss', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directores`
--

CREATE TABLE `directores` (
  `IdDirector` int(11) NOT NULL,
  `NomDirector` varchar(50) NOT NULL,
  `Idusuario` int(11) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `NomDepartamento` enum('CFG','Comisión Revisora','DEA','Vicerrectoría Académica','CUDE','Direcciones de Escuelas','Gestión Humana','Aseguramiento de la Calidad','CINGEP','Investigación Científica') NOT NULL,
  `Estado` enum('Activo','Inactivo','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `directores`
--

INSERT INTO `directores` (`IdDirector`, `NomDirector`, `Idusuario`, `Correo`, `NomDepartamento`, `Estado`) VALUES
(7, 'Jovanny Rodríguez', 0, 'Jovanny_Rodríguez@gmail.com', 'Vicerrectoría Académica', 'Activo'),
(12, 'Reyna', 0, 'reyna@gmail.com', 'CINGEP', 'Activo'),
(15, 'Pedro Emilio', 0, 'Pedroo@gmail.com', 'CFG', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ejes`
--

CREATE TABLE `ejes` (
  `IdEjes` int(11) NOT NULL,
  `NomEjes` varchar(30) NOT NULL,
  `DescEjes` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ejes`
--

INSERT INTO `ejes` (`IdEjes`, `NomEjes`, `DescEjes`) VALUES
(15, 'FP', 'Formación Profesional'),
(17, 'IN', ' Investigación e Innovación');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evidencia`
--

CREATE TABLE `evidencia` (
  `Idevidencia` int(11) NOT NULL,
  `id_detalle` int(11) NOT NULL,
  `Foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `img_temp`
--

CREATE TABLE `img_temp` (
  `id` int(15) NOT NULL,
  `id_persona` int(15) NOT NULL,
  `name` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `indicadores`
--

CREATE TABLE `indicadores` (
  `Idindicador` int(11) NOT NULL,
  `Idactividades` int(11) NOT NULL,
  `Idmeta` varchar(30) NOT NULL,
  `Idobjtacticos` varchar(30) NOT NULL,
  `NomDirector` varchar(30) NOT NULL,
  `Fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineas_actuacion`
--

CREATE TABLE `lineas_actuacion` (
  `Idlinactuacion` int(11) NOT NULL,
  `Idobjtactico` int(11) NOT NULL,
  `Nomlinactuacion` varchar(30) NOT NULL,
  `DescLinactuacion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `lineas_actuacion`
--

INSERT INTO `lineas_actuacion` (`Idlinactuacion`, `Idobjtactico`, `Nomlinactuacion`, `DescLinactuacion`) VALUES
(1, 2, 'FP- 1.1', 'Realización de estudios de mercados para la evaluación y mejora de los planes de estudios de grado y postgrado.'),
(2, 3, 'FP-1.2', 'Diversificación de la oferta académica de grado vigente, en consonancia con las necesidades y demandas de la sociedad.'),
(3, 4, 'FP 2.1 ', 'Garantía de la calidad pedagógica de la docencia.'),
(4, 5, '1.3 ', 'Desarrollo de la investigación e innovación en áreas temáticas de interés a nivel local y regional.'),
(5, 5, '1.4 ', 'Fomento de la producción científica a nivel nacional e internacional.'),
(6, 5, '2.3 ', 'Desarrollo de proyectos emprendedores en el programa de incubación del CUDE.'),
(7, 7, 'FP 2.1 ', 'Garantía de la calidad pedagógica de la docencia.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metas`
--

CREATE TABLE `metas` (
  `Idmeta` int(11) NOT NULL,
  `Idlinactuacion` int(11) NOT NULL,
  `Nommeta` varchar(30) NOT NULL,
  `Descmetas` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `metas`
--

INSERT INTO `metas` (`Idmeta`, `Idlinactuacion`, `Nommeta`, `Descmetas`) VALUES
(2, 1, '1', 'Realización de un  diagnóstico para evaluar la situación académica real del C.F.G.'),
(3, 1, '2', 'Modificación del Reglamento del C.F.G en correspondencia con las actualizaciones de la Universidad.'),
(6, 1, '3', 'Elaboración de un protocolo de evaluación del CFG a partir de los Reglamentos institucionales.'),
(7, 2, '1', 'Diseño de un protocolo para la elaboración, evaluación e implementación de los Diplomados del C.F.G. '),
(8, 7, '1', 'Creación de un procedimiento funcional para la contratación de facilitadores en el CFG y crear un banco de elegibles.'),
(9, 7, '2', 'Gestionar el diseño de al menos 5 cursos virtuales para CFG de acuerdo al MECCA. (Uno genérico por Escuela)\r\n\r\n'),
(10, 4, '1', 'Elaboración de  al menos 5 artículos científicos resultado de proyectos desarrollados en el CFG.'),
(11, 5, '1', 'Elaboración de  al menos 5 artículos científicos resultado de proyectos desarrollados en el CFG.'),
(12, 6, '1', 'Tramitación con el CUDE para apoyar a por lo menos 5 proyectos emprendedores del CFG.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obj_estratejicos`
--

CREATE TABLE `obj_estratejicos` (
  `Idobjestratejico` int(11) NOT NULL,
  `IdEjes` int(11) NOT NULL,
  `nomobjestratejico` varchar(30) NOT NULL,
  `descobjestratejico` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `obj_estratejicos`
--

INSERT INTO `obj_estratejicos` (`Idobjestratejico`, `IdEjes`, `nomobjestratejico`, `descobjestratejico`) VALUES
(2, 15, 'FP-I', 'Mejorar, Transformar y diversificar la oferta académica de grado y postgrado vigentes, en coherencia con las necesidades y demandas de la sociedad.'),
(6, 16, 'FP-II', ' Asegurar la calidad científica, pedagógica y tecnológica de la docencia, para garantizar la pertenencia de las titulaciones de grado y postgrado.'),
(7, 17, 'IN-I', 'Fomentar y potenciar la investigación y la innovación como soporte de la calidad de la formación académica y de contribución efectiva al desarrollo y progreso de la sociedad.'),
(8, 15, 'FP-II', 'Asegurar la calidad científica, pedagógica y tecnológica de la docencia, para garantizar la pertenencia de las titulaciones de grado y postgrado.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obj_tactico`
--

CREATE TABLE `obj_tactico` (
  `Idobjtactico` int(11) NOT NULL,
  `Idobjestratejico` int(11) NOT NULL,
  `Nomobjtactico` varchar(30) NOT NULL,
  `descobjtactico` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `obj_tactico`
--

INSERT INTO `obj_tactico` (`Idobjtactico`, `Idobjestratejico`, `Nomobjtactico`, `descobjtactico`) VALUES
(2, 2, 'FP-1.1.3 ', 'Implementar mejoras en el diseño de los Diplomados de Curso Final de Grado en función a los Requerimientos de formación del mercado laboral.'),
(3, 2, 'FP- 1.2.2 ', 'Diseñar nuevos Diplomados de Curso Final de Grado de acuerdo a las necesidades de formativas de los participantes y de las demandas del laboral.'),
(4, 6, 'FP-2.1.4 ', 'Crear un banco de especialistas elegibles como posibles facilitadores del CFG, cumpliendo con las normativas de reclutamiento establecidas por la Institución.'),
(5, 7, 'IN-1.3.4 ', 'Involucrar a los facilitadores y participantes en la elaboración y ejecución de proyectos de investigación e innovación.'),
(7, 8, 'FP-2.1.4 ', 'Crear un banco de especialistas elegibles como posibles facilitadores del CFG, cumpliendo con las normativas de reclutamiento establecidas por la Institución.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `Idrol` int(11) NOT NULL,
  `Rol` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`Idrol`, `Rol`) VALUES
(1, 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea_actividades`
--

CREATE TABLE `tarea_actividades` (
  `Idtareaactividades` int(11) NOT NULL,
  `Idactividades` int(11) NOT NULL,
  `DescTarea` varchar(300) NOT NULL,
  `EstadoTarea` enum('Completado','Enproceso','Declinado','') DEFAULT NULL,
  `progreso` int(3) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tarea_actividades`
--

INSERT INTO `tarea_actividades` (`Idtareaactividades`, `Idactividades`, `DescTarea`, `EstadoTarea`, `progreso`) VALUES
(2, 2, 'Consultar a participantes egresados, facilitadores y directores de Escuela.', 'Enproceso', 0),
(3, 2, 'buscador', 'Declinado', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea_detalle`
--

CREATE TABLE `tarea_detalle` (
  `id_detalle` int(11) NOT NULL,
  `Idtareaactividades` int(11) NOT NULL,
  `accion` text DEFAULT NULL,
  `estado` varchar(30) NOT NULL,
  `progreso` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `Idusuario` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `Usuario` varchar(15) NOT NULL,
  `password` varchar(150) NOT NULL,
  `Idrol` int(11) NOT NULL,
  `estado` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`Idusuario`, `Nombre`, `email`, `Usuario`, `password`, `Idrol`, `estado`) VALUES
(2, 'admin', 'admin@admin.com', 'admin', '$2a$07$asxx54ahjppf45sd87a5auXBm1Vr2M1NV5t/zNQtGHGpS5fFirrbG', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`Idactividades`),
  ADD KEY `Idmeta` (`Idmeta`);

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`id_bitacora`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`IdDeparta`);

--
-- Indices de la tabla `directores`
--
ALTER TABLE `directores`
  ADD PRIMARY KEY (`IdDirector`);

--
-- Indices de la tabla `ejes`
--
ALTER TABLE `ejes`
  ADD PRIMARY KEY (`IdEjes`);

--
-- Indices de la tabla `evidencia`
--
ALTER TABLE `evidencia`
  ADD PRIMARY KEY (`Idevidencia`),
  ADD KEY `Idtareaactividades` (`id_detalle`);

--
-- Indices de la tabla `img_temp`
--
ALTER TABLE `img_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `indicadores`
--
ALTER TABLE `indicadores`
  ADD PRIMARY KEY (`Idindicador`);

--
-- Indices de la tabla `lineas_actuacion`
--
ALTER TABLE `lineas_actuacion`
  ADD PRIMARY KEY (`Idlinactuacion`),
  ADD KEY `Idobjtactico` (`Idobjtactico`);

--
-- Indices de la tabla `metas`
--
ALTER TABLE `metas`
  ADD PRIMARY KEY (`Idmeta`),
  ADD KEY `Idlinactuacion` (`Idlinactuacion`);

--
-- Indices de la tabla `obj_estratejicos`
--
ALTER TABLE `obj_estratejicos`
  ADD PRIMARY KEY (`Idobjestratejico`),
  ADD KEY `IdEjes` (`IdEjes`);

--
-- Indices de la tabla `obj_tactico`
--
ALTER TABLE `obj_tactico`
  ADD PRIMARY KEY (`Idobjtactico`),
  ADD KEY `Idobjestratejico` (`Idobjestratejico`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`Idrol`);

--
-- Indices de la tabla `tarea_actividades`
--
ALTER TABLE `tarea_actividades`
  ADD PRIMARY KEY (`Idtareaactividades`),
  ADD KEY `Idactividades` (`Idactividades`);

--
-- Indices de la tabla `tarea_detalle`
--
ALTER TABLE `tarea_detalle`
  ADD PRIMARY KEY (`id_detalle`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividad`
--
ALTER TABLE `actividad`
  MODIFY `Idactividades` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `id_bitacora` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=730;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `IdDeparta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `directores`
--
ALTER TABLE `directores`
  MODIFY `IdDirector` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `ejes`
--
ALTER TABLE `ejes`
  MODIFY `IdEjes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `evidencia`
--
ALTER TABLE `evidencia`
  MODIFY `Idevidencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `img_temp`
--
ALTER TABLE `img_temp`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `indicadores`
--
ALTER TABLE `indicadores`
  MODIFY `Idindicador` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lineas_actuacion`
--
ALTER TABLE `lineas_actuacion`
  MODIFY `Idlinactuacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `metas`
--
ALTER TABLE `metas`
  MODIFY `Idmeta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `obj_estratejicos`
--
ALTER TABLE `obj_estratejicos`
  MODIFY `Idobjestratejico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `obj_tactico`
--
ALTER TABLE `obj_tactico`
  MODIFY `Idobjtactico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `Idrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tarea_actividades`
--
ALTER TABLE `tarea_actividades`
  MODIFY `Idtareaactividades` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `tarea_detalle`
--
ALTER TABLE `tarea_detalle`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
